# dr_troittoir_api
Api written in ASP.NET

## Project Description
In dit project was het de bedoeling een dashboard te maken voor het bedrijf DrTrottoir. Dit is een bedrijf waarbij studenten de nodige afval buiten zetten van meerdere grote gebouwen en deze indien nodig terug binnen zetten. Hiervoor was er een heel systeem nodig waarbij routes en gebouwen kunnen worden aangemaakt zodat studenten hierop kunnen toegewezen worden. Een methode om de planning van de afvalophaling per gebouw eenvoudig in te geven was hierbij ook nodig. Voor de uitwerking hiervan hebben wij gekozen voor Vue als frontend-framework en ASP.Net core als bakcend.
Een bijkomend project was een app voor deze studenten om hun toegewezen routes te kunnen zien, met een lijst van gebouwen met hun instructies en velden om foto's te maken en direct up te loaden als bewijs. Dit werd gedaan in Flutter. (Hieraan heb ik niet gewerkt, dus heb ik deze niet toegevoegd aan dit portfolio)

## How to run
#### Project setup
```sh
dotnet restore
```

#### Run project
```sh
dotnet run
```

## Project structure
| Project   | Description                                                            |
|-----------|------------------------------------------------------------------------|
| Api       | Main project serving api                                               |
| Logic     | Logic layers                                                           |
| Models    | Solution global models, used by EfCore                                 |
| Resources | External resource implementations, EfCore, File Bucket, Tabel Storages |
| UnitTests | Unit tests                                                             |

