using Api.Models;
using Api.StartupExtensions;
using Logic;
using Logic.Implementations;
using Resources;
using Resources.Context;
using Resources.Implementations;

var builder = WebApplication.CreateBuilder(args);
var services = builder.Services;
var configuration = builder.Configuration;
// builder.AddKeyVault();

services.AddDatabase(configuration);
services.AddAuth(configuration);
// services.AddApplicationInsightsTelemetry();
services.AddCorsPolicy(configuration);

services.AddControllers();

if (builder.Environment.IsDevelopment())
{
    //add swagger
    services.AddSwaggerGen(c =>
    {
        c.SwaggerDoc("v1", new() { Title = "Api", Version = "v1" });
    });
}

services.AddAutoMapper(typeof(ApiProfile));

// Logic
services.AddScoped<IAuthLogic, AuthLogic>();
services.AddScoped<IUserLogic, UserLogic>();
services.AddScoped<IRegionLogic, RegionLogic>();
services.AddScoped<IRoundLogic, RoundLogic>();
services.AddScoped<IBuildingLogic, BuildingLogic>();
services.AddScoped<IImageLogic, ImageLogic>();
services.AddScoped<IRoundGeneratorLogic, RoundGeneratorLogic>();

// Resources
services.AddScoped<ISmsResource, DiscordHookSmsStandIn>();
services.AddScoped<ITableResource, RedisCacheTableStorage>();
services.AddScoped<IBlobStorage, GoogleCloudBucket>();

var app = builder.Build();

app.UseAuthentication();
app.UseAuthorization();
app.MapControllers();
app.EnsureEfMigration<DtContext>();
app.UseCorsPolicy();

if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Api v1"));
}

app.Run();