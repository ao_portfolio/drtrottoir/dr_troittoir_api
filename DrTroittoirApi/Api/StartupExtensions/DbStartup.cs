﻿using Microsoft.EntityFrameworkCore;
using Resources.Context;

namespace Api.StartupExtensions;

public static class DbStartup
{
    public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
    {
        var connectionString = configuration.GetConnectionString("SqlServerConnection");

        services
            .AddDbContext<DtContext>(options => options.UseNpgsql(connectionString))
            ;

        return services;
    }
    
    public static IApplicationBuilder EnsureEfMigration<T>(this IApplicationBuilder app) where T : DbContext
    {
        using var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope();
        var context = serviceScope.ServiceProvider.GetRequiredService<T>();
        context.Database.Migrate();
        return app;
    }
}