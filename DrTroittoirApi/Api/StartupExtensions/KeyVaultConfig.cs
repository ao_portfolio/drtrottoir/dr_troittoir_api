﻿using System.Security.Cryptography.X509Certificates;
using Azure.Identity;
using Models.Extensions;
using Resources.Extensions;

namespace Api.StartupExtensions;

public static class KeyVaultConfig
{
    public static WebApplicationBuilder AddKeyVault(this WebApplicationBuilder builder)
    {
        var secret = GetSecretForEnvironment(builder);
        if (string.IsNullOrEmpty(secret))
            return builder;
        
        
        builder.Configuration.AddAzureKeyVault(
            new Uri($"https://{builder.Configuration["Vault:KeyVaultName"]}.vault.azure.net/"),
            new ClientSecretCredential(builder.Configuration["Vault:AzureADDirectoryId"], builder.Configuration["Vault:AzureADApplicationId"], secret));
        
        return builder;
    }

    private static string? GetSecretForEnvironment(this WebApplicationBuilder builder)
    {
        if(builder.Configuration["Vault:Secret"].IsNotNullOrEmpty())
            return builder.Configuration["Vault:Secret"];
        
        var environment = builder.Configuration["Environment"];
        
        switch (environment)
        {
            case "Development":
                return builder.Configuration["Vault:SecretDev"];
            case "Staging":
                return builder.Configuration["Vault:SecretStaging"];
            case "Production":
                return builder.Configuration["Vault:SecretPrd"];
        }
        
        return null;
    }
}