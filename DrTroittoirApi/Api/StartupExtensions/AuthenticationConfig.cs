﻿using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.IdentityModel.Tokens;
using Resources.Extensions;

namespace Api.StartupExtensions;

public static class AuthenticationConfig
{
    public static IServiceCollection AddAuth(this IServiceCollection services, IConfiguration configuration)
    {
        services
            .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
            .AddJwtBearer(JwtBearerDefaults.AuthenticationScheme, options =>
            {
                options.TokenValidationParameters = new TokenValidationParameters
                {
                    ValidIssuer = configuration.GetRequired("Jwt:Issuer"),
                    ValidAudience = configuration.GetRequired("Jwt:Audience"),
                    IssuerSigningKey = new SymmetricSecurityKey
                        (Encoding.UTF8.GetBytes(configuration.GetRequired("Jwt:Key"))),
                    ValidateIssuer = true,
                    ValidateAudience = true,
                    ValidateLifetime = false,
                    ValidateIssuerSigningKey = true
                };
                
                options.Events = new JwtBearerEvents
                {
                    OnMessageReceived = context =>
                    {
                        if (context.HttpContext.RequestServices.GetService(typeof(IAuthorizationService)) is
                            not IAuthorizationService authService) 
                            return Task.CompletedTask;
                        
                        if (!context.Request.Cookies.ContainsKey("JWT")) 
                            return Task.CompletedTask;
                        
                        var token = context.Request.Cookies["jwt"];
                        context.Token = token;
                        
                        return Task.CompletedTask;
                    }
                };
            })
            ;

        return services;
    }
}