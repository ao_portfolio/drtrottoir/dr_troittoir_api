﻿using System.ComponentModel.DataAnnotations;
using AutoMapper;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
public class RegionController : ControllerBase
{
    private readonly IRegionLogic _regionLogic;
    private readonly IMapper _mapper;
    
    public RegionController(IRegionLogic regionLogic, IMapper mapper)
    {
        _regionLogic = regionLogic;
        _mapper = mapper;
    }
    
    [HttpGet]
    [Route("/api/region")]
    [Authorize]
    public async Task<IActionResult> GetRegions()
    {
        var regions = await _regionLogic.GetRegionsAsync();
        var response = _mapper.Map<RegionResponse[]>(regions);
        return Ok(response);
    }
    
    [HttpPost]
    [Route("/api/region")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> CreateRegion(CreateRegionRequest request)
    {
        var region = _mapper.Map<Region>(request);
        var responseRegion = await _regionLogic.CreateRegionAsync(region);
        
        var response = _mapper.Map<RegionResponse>(responseRegion);
        return Ok(response);
    }
    
    [HttpDelete]
    [Route("/api/region/{id}")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> DeleteRegion(string id)
    {
        await _regionLogic.DeleteRegionAsync(id);
        return Ok();
    }
    
    [HttpPut]
    [Route("/api/region")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> UpdateRegion(UpdateRegionRequest request)
    {
        var region = _mapper.Map<Region>(request);
        var responseRegion = await _regionLogic.UpdateRegionAsync(region);
        
        var response = _mapper.Map<RegionResponse>(responseRegion);
        return Ok(response);
    }
}

public class UpdateRegionRequest
{
    public string Id { get; set; }
    public string Name { get; set; }
}

public class CreateRegionRequest
{
    public string Name { get; set; }
}

public class RegionResponse
{
    public string Id { get; set; }
    public string Name { get; set; }
}