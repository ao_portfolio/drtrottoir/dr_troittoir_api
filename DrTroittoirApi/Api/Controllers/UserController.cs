﻿using Api.Extensions;
using AutoMapper;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
[Authorize]
public class UserController : ControllerBase
{
    private readonly IUserLogic _userLogic;
    private readonly IMapper _mapper;
    private readonly IRegionLogic _regionLogic;
    
    public UserController(IUserLogic userLogic, IMapper mapper, IRegionLogic regionLogic)
    {
        _userLogic = userLogic;
        _mapper = mapper;
        _regionLogic = regionLogic;
    }
    
    [HttpGet]
    [Route("/api/user")]
    [Authorize]
    public async Task<IActionResult> GetUser()
    {
        var user = await _userLogic.GetUserByPhone(User.Identity.Name);
        var response = _mapper.Map<UserResponse>(user);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/user/all")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> GetAllUsers()
    {
        var users = await _userLogic.GetAllUsers();
        var response = _mapper.Map<UserResponse[]>(users);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/user/id/{id}")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> GetUserById(string id)
    {
        var user = await _userLogic.GetUserById(id);
        var response = _mapper.Map<UserResponse>(user);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/user/phone/{phoneNumber}")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> GetUserByPhoneNumber(string phoneNumber)
    {
        var user = await _userLogic.GetUserByPhone(phoneNumber);
        var response = _mapper.Map<UserResponse>(user);
        return Ok(response);
    }
    
    
    [HttpPost]
    [Route("/api/user")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> CreateUser(CreateUserRequest request)
    {
        var user = _mapper.Map<User>(request);
        var responseUser = await _userLogic.AddUser(user);
        
        var response = _mapper.Map<UserResponse>(responseUser);
        return Ok(response);
    }
    
    [HttpPut]
    [Route("/api/user")]
    [Authorize]
    public async Task<IActionResult> UpdateUser(UpdateUserRequest request)
    {
        var user = await _userLogic.GetUserByPhone(User.UserPhoneNumber());
        if(User.IsAdmin())
            return await UpdateUserAsAdmin(request, user);
        
        if(user.Id != request.Id)
            return BadRequest("You can only update your own user");
        
        return await UpdateUserSelf(request, user);
    }

    private async Task<IActionResult> UpdateUserAsAdmin(UpdateUserRequest request, User user)
    {
        await UpdateBasicUserInfo(request, user);

        var responseUser = await _userLogic.UpdateUser(user);
        var response = _mapper.Map<UserResponse>(responseUser);
        return Ok(response);
    }

    private async Task<IActionResult> UpdateUserSelf(UpdateUserRequest request, User user)
    {
        await UpdateBasicUserInfo(request, user);

        var responseUser = await _userLogic.UpdateUser(user);
        var response = _mapper.Map<UserResponse>(responseUser);
        return Ok(response);
    }
    
    private async Task UpdateBasicUserInfo(UpdateUserRequest request, User user)
    {
        user.FirstName = request.FirstName;
        user.LastName = request.LastName;
        user.Email = request.Email;
        user.PhoneNumber = request.PhoneNumber;
        
        user.Regions.Clear();
        user.Regions = await _regionLogic.GetRegionsAsync(request.RegionIds);
    }

}

public class UpdateUserRequest
{
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public string? Email { get; set; }
    public List<string> RegionIds { get; set; }
}

public class UserResponse
{
    public string Id { get; set; }
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Name { get; set; }
    public string Email { get; set; }
    public string PhoneNumber { get; set; }
    public string Role { get; set; }
    public List<RegionResponse> Regions { get; set; }
}

public class CreateUserRequest
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string PhoneNumber { get; set; }
    public Role Role { get; set; }
    public string? Email { get; set; }
}   