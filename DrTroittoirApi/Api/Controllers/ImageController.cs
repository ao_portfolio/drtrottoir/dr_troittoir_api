﻿using Api.Extensions;
using AutoMapper;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
public class ImageController : ControllerBase
{
    private readonly IImageLogic _imageLogic;
    private readonly IMapper _mapper;
    
    public ImageController(IImageLogic imageLogic, IMapper mapper)
    {
        _imageLogic = imageLogic;
        _mapper = mapper;
    }
    
    [HttpPost]
    [Route("/api/image")]
    [Authorize]
    public async Task<IActionResult> UploadImage([FromForm(Name = "file")] IFormFile file)
    {
        await using var stream = file.OpenReadStream();
        var image = await _imageLogic.UploadImageAsync(stream, file.FileName, User.UserId());
        var response = _mapper.Map<ImageResponse>(image);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/image")]
    [Authorize]
    public async Task<IActionResult> RedirectToBlobUrl(Uri uri, string name)
    {
        var redirect = await _imageLogic.GetImageUrlWithToken(uri.ToString(), name);
        return Redirect(redirect);
    }
    
    [HttpGet]
    [Route("/api/image/recent")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> GetRecentImages()
    {
        var images = await _imageLogic.GetRecentImages();
        var response = _mapper.Map<ImageResponseDetail[]>(images);
        return Ok(response);
    }
    

}

public class ImageResponseDetail
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string OriginalUrl { get; set; }
    public string ThumbnailUrl { get; set; }
    public string UploaderId { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? ExpiresAt { get; set; }
}

public class ImageResponse
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string OriginalUrl { get; set; }
    public string ThumbnailUrl { get; set; }
}