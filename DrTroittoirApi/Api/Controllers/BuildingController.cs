﻿using AutoMapper;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
[Authorize]
public class BuildingController : ControllerBase
{
    private readonly IBuildingLogic _buildingLogic;
    private readonly IMapper _mapper;
    
    public BuildingController(IBuildingLogic buildingLogic, IMapper mapper)
    {
        _buildingLogic = buildingLogic;
        _mapper = mapper;
    }
    
    [HttpGet]
    [Route("/api/building")]
    [Authorize]
    public async Task<IActionResult> GetBuildings()
    {
        var buildings = await _buildingLogic.GetBuildingsAsync();
        var response = _mapper.Map<BuildingResponse[]>(buildings);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/building/{id}")]
    [Authorize]
    public async Task<IActionResult> GetBuilding(string id)
    {
        var building = await _buildingLogic.GetBuildingByIdAsync(id);
        var response = _mapper.Map<BuildingDetailsResponse>(building);
        return Ok(response);
    }

    [HttpGet]
    [Route("/api/building/geocode/{query}")]
    [Authorize]
    public async Task<IActionResult> Geocode(string query)
    {
        var response = await _buildingLogic.Geocode(query);
        return Ok(response);
    }

    [HttpPost]
    [Route("/api/building")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> CreateBuilding(CreateBuildingRequest request)
    {
        var building = _mapper.Map<Building>(request);
        var responseBuilding = await _buildingLogic.CreateBuildingAsync(building);
        
        var response = _mapper.Map<BuildingDetailsResponse>(responseBuilding);
        return Ok(response);
    }
    
    [HttpDelete]
    [Route("/api/building/{id}")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> DeleteBuilding(string id)
    {
        await _buildingLogic.DeleteBuildingAsync(id);
        return Ok();
    }
    
    [HttpPut]
    [Route("/api/building")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> UpdateBuilding(UpdateBuildingRequest request)
    {
        var building = _mapper.Map<Building>(request);
        var responseBuilding = await _buildingLogic.UpdateBuildingAsync(building);
        
        var response = _mapper.Map<BuildingResponse>(responseBuilding);
        return Ok(response);
    }
}

public class UpdateBuildingRequest
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string RoundId { get; set; }
    public string? ImageId { get; set; }
    public string Address { get; set; }
    public string? Description { get; set; }
    public string? City { get; set; }
    public string? Zip { get; set; }
    public decimal? Lat { get; set; }
    public decimal? Lon { get; set; }
    public string? InstructionsString { get; set; }
    public string? ScheduleString { get; set; }
}

public class CreateBuildingRequest
{
    public string Name { get; set; }
    public string RoundId { get; set; }
    public string? ImageId { get; set; }
    public string Address { get; set; }
    public string? Description { get; set; }
    public string? City { get; set; }
    public string? Zip { get; set; }
    public decimal? Lat { get; set; }
    public decimal? Lon { get; set; }
    public string? InstructionsString { get; set; }
    public string? ScheduleString { get; set; }
}

public class BuildingResponse
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public string? Description { get; set; }
    public RoundResponse Round { get; set; }
    public string? City { get; set; }
    public string? Zip { get; set; }
    public decimal? Lat { get; set; }
    public decimal? Lon { get; set; }
    public string? InstructionsString { get; set; }
    public string? ScheduleString { get; set; }
}

public class BuildingDetailsResponse
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string Address { get; set; }
    public string? Description { get; set; }
    public RoundResponse Round { get; set; }
    public ImageResponse Image { get; set; }
    public string? City { get; set; }
    public string? Zip { get; set; }
    public decimal? Lat { get; set; }
    public decimal? Lon { get; set; }
    public string? InstructionsString { get; set; }
    public string? ScheduleString { get; set; }
}