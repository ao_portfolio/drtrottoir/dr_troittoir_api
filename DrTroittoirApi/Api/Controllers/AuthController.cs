﻿using Api.Extensions;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Api.Controllers;

[ApiController]
public class AuthController : ControllerBase
{
    private readonly IAuthLogic _authLogic;

    public AuthController(IAuthLogic authLogic)
    {
        _authLogic = authLogic;
    }

    [HttpPost]
    [AllowAnonymous]
    [ProducesResponseType(typeof(AuthSessionResponse), 200)]
    [Route("/api/auth")]
    public async Task<IActionResult> CreateAuthSession(AuthSessionRequest request)
    {
        var session = await _authLogic.CreateAuthSessionAsync(request.PhoneNumber);
        var response = new AuthSessionResponse
        {
            PhoneNumber = session.PhoneNumber,
            ClientSecret = session.ClientSecret,
            ExpiresAt = session.ExpiresAt
        };

        return Ok(response);
    }

    [HttpPost]
    [AllowAnonymous]
    [Route("/api/auth/complete")]
    public async Task<IActionResult> CompleteAuthSession(AuthSessionCompleteRequest request)
    {
        var valid = await _authLogic.CompleteAuthSessionAsync(request.PhoneNumber, request.ClientSecret, request.Code);

        if (!valid)
            return BadRequest();

        (string token, string refreshToken) = await _authLogic.CreateTokenSessionForUser(request.PhoneNumber, "test");
        
        AddTokenAsCookie(token);

        return Ok(new
        {
            token,
            refreshToken
        });
    }

    [HttpPost]
    [AllowAnonymous]
    [Route("/api/auth/refresh")]
    public async Task<IActionResult> RefreshToken(RefreshTokenRequest request)
    {
        if (string.IsNullOrEmpty(request.Token) || string.IsNullOrEmpty(request.RefreshToken))
            return BadRequest();

        var principal = _authLogic.GetPrincipalFromExpiredToken(request.Token);
        var phoneNumber = principal.Identity.Name;

        if (string.IsNullOrEmpty(phoneNumber))
            return BadRequest();

        (string token, string refreshToken) =
            await _authLogic.ExtentTokenSessionForUser(phoneNumber, "test", request.RefreshToken);
        
        AddTokenAsCookie(token);

        return Ok(new
        {
            token,
            refreshToken
        });
    }


    [HttpGet]
    [Authorize]
    [Route("/api/auth/validate")]
    public IActionResult Validate()
    {
        return Ok();
    }
    
    private void AddTokenAsCookie(string token)
    {
        Response.Cookies.Append("JWT", token, new CookieOptions
        {
            HttpOnly = true,
            Secure = true,
            Expires = DateTime.UtcNow.AddDays(7)
        });
    }
    
    [HttpGet]
    [Authorize]
    [Route("/api/auth/logout")]
    public async Task<IActionResult> Logout()
    {
        var token = "";
        
        if (Request.Cookies.ContainsKey("JWT"))
            token = Request.Cookies["JWT"];
        
        if(Request.Headers.ContainsKey("Authorization"))
            token = Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
        
        await _authLogic.Logout(User.UserId(), token);
        Response.Cookies.Delete("JWT");
        return Ok();
    }
    
}

public class RefreshTokenRequest
{
    public string Token { get; set; }
    public string RefreshToken { get; set; }
}

public class AuthSessionCompleteRequest
{
    public string PhoneNumber { get; set; }
    public string ClientSecret { get; set; }
    public string Code { get; set; }
}

public class AuthSessionRequest
{
    public string PhoneNumber { get; set; }
}

public class AuthSessionResponse
{
    public string PhoneNumber { get; set; }
    public string ClientSecret { get; set; }
    public DateTime ExpiresAt { get; set; }
}