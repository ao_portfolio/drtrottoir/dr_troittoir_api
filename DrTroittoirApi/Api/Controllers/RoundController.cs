﻿using AutoMapper;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
[Authorize]
public class RoundController : ControllerBase
{
    private readonly IRoundLogic _roundLogic;
    private readonly IMapper _mapper;
    
    public RoundController(IRoundLogic roundLogic, IMapper mapper)
    {
        _roundLogic = roundLogic;
        _mapper = mapper;
    }
    
    [HttpGet]
    [Route("/api/round")]
    [Authorize]
    public async Task<IActionResult> GetRounds()
    {
        var rounds = await _roundLogic.GetRoundsAsync(false);
        var response = _mapper.Map<RoundResponse[]>(rounds);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/round/{id}")]
    [Authorize]
    public async Task<IActionResult> GetRound(string id)
    {
        var round = await _roundLogic.GetRoundByIdAsync(id, false);
        var response = _mapper.Map<RoundResponse>(round);
        return Ok(response);
    }
    
    [HttpPost]
    [Route("/api/round")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> CreateRound(CreateRoundRequest request)
    {
        var round = _mapper.Map<Round>(request);
        var responseRound = await _roundLogic.CreateRoundAsync(round);
        
        var response = _mapper.Map<RoundResponse>(responseRound);
        return Ok(response);
    }
    
    [HttpDelete]
    [Route("/api/round/{id}")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> DeleteRound(string id)
    {
        await _roundLogic.DeleteRoundAsync(id);
        return Ok();
    }
    
    [HttpPut]
    [Route("/api/round")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> UpdateRound(UpdateRoundRequest request)
    {
        var round = _mapper.Map<Round>(request);
        var responseRound = await _roundLogic.UpdateRoundAsync(round);
        
        var response = _mapper.Map<RoundResponse>(responseRound);
        return Ok(response);
    }
    
    
}

public class UpdateRoundRequest
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string RegionId { get; set; }
}

public class CreateRoundRequest
{
    public string Name { get; set; }
    public string RegionId { get; set; }
}

public class RoundResponse
{
    public string Id { get; set; }
    public string Name { get; set; }
    public RegionResponse Region { get; set; }
    public string[] BuildingIds { get; set; }
    public string[] AssignedUserIds { get; set; }
}