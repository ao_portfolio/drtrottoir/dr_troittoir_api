﻿using Api.Extensions;
using AutoMapper;
using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
[Authorize]
public class RoundGeneratorController : ControllerBase
{
    private readonly IRoundLogic _roundLogic;
    private readonly IRoundGeneratorLogic _roundGeneratorLogic;
    private readonly IMapper _mapper;

    public RoundGeneratorController(IRoundLogic roundLogic, IMapper mapper, IRoundGeneratorLogic roundGeneratorLogic)
    {
        _roundLogic = roundLogic;
        _mapper = mapper;
        _roundGeneratorLogic = roundGeneratorLogic;
    }


    [HttpGet]
    [Route("/api/myRounds")]
    [Authorize]
    public async Task<IActionResult> GetMyRounds()
    {
        var myRounds = await _roundLogic.MyRounds(User.UserId());
        var response = _mapper.Map<RoundResponse[]>(myRounds);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/myRounds/today")]
    [Authorize]
    public async Task<IActionResult> GetMyRoundsForToday()
    {
        var myRounds = await _roundGeneratorLogic.MyRoundsForDay(User.UserId(), DateTime.Now);
        var response = _mapper.Map<RoundResponse[]>(myRounds);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/myRounds/offset/{offset:int}")]
    [Authorize]
    public async Task<IActionResult> GetMyRounds(int offset)
    {
        var myRounds = await _roundGeneratorLogic.MyRoundsForDay(User.UserId(), DateTime.Now.AddDays(offset));
        var response = _mapper.Map<RoundResponse[]>(myRounds);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/startRound/{roundId}")]
    [Authorize]
    public async Task<IActionResult> StartRound(string roundId)
    {
        var round = await _roundGeneratorLogic.StartRound(User.UserId(), roundId, DateTime.Now.AddDays(-2));
        var response = _mapper.Map<RoundSessionResponse>(round);
        return Ok(response);
    }
    
    [HttpGet]
    [Route("/api/garbageInfo/round/{roundId}")]
    [Authorize]
    public async Task<IActionResult> GetGarbageInfo(string roundId)
    {
        var schedule = await _roundGeneratorLogic.GetGarbageScheduleForDay(User.UserId(), roundId, DateTime.Now.AddDays(-2));
        return Ok(schedule);
    }
}

public class RoundSessionResponse
{
    public string? RoundId { get; set; }
    public RoundSnapshotResponse? RoundSnapshot { get; set; }
    public DateTime? StartedAt { get; set; }
    public DateTime? CompletedAt { get; set; }
    public bool IsCompleted => CompletedAt.HasValue;
}

public class RoundSnapshotResponse
{
    public DateTime SnapShotAt { get; set; }
    public string RoundInfo { get; set; }
    public string Hash { get; set; }
}



