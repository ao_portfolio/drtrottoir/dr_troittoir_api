﻿using Logic;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Models;

namespace Api.Controllers;

[ApiController]
[Authorize]
public class AssignController : ControllerBase
{
    private readonly IRoundLogic _roundLogic;

    public AssignController(IRoundLogic roundLogic)
    {
        _roundLogic = roundLogic;
    }

    [HttpPost]
    [Route("/api/assign/route")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> AssignRoute(AssignRouteRequest request)
    {
        await _roundLogic.AssignUserToRound(request.RoundId, request.UserId);
        return Ok();
    }
    
    [HttpPost]
    [Route("/api/unassign/route")]
    [Authorize(Roles = nameof(Role.Admin))]
    public async Task<IActionResult> UnAssignRoute(AssignRouteRequest request)
    {
        await _roundLogic.UnAssignUserFromRound(request.RoundId, request.UserId);
        return Ok();
    }
}

public class AssignRouteRequest
{
    public string RoundId { get; set; }
    public string UserId { get; set; }
}