﻿using Api.Controllers;
using AutoMapper;
using Models;

namespace Api.Models;

public class ApiProfile : Profile
{
    public ApiProfile()
    {
        CreateMap<User, UserResponse>()
            .ForMember(x => x.Role, opt => opt.MapFrom(x => x.Role.ToString()))
            ;
        
        CreateMap<CreateUserRequest, User>();
        
        // Regions
        CreateMap<Region, RegionResponse>();
        CreateMap<UpdateRegionRequest, Region>();
        CreateMap<CreateRegionRequest, Region>()
            .ForMember(e => e.Id, opt => opt.Ignore())
            ;
        
        // Rounds
        CreateMap<Round, RoundResponse>()
            .ForMember(e => e.BuildingIds, opt => opt.MapFrom(e => e.Buildings.Select(r => r.Id)))
            .ForMember(e => e.AssignedUserIds, opt => opt.MapFrom(e => e.AssignedUsers.Select(r => r.Id)))
            ;
        CreateMap<CreateRoundRequest, Round>()
            .ForMember(e => e.Id, opt => opt.Ignore())
            ;
        CreateMap<UpdateRoundRequest, Round>();
        
        // Buildings
        CreateMap<Building, BuildingResponse>();
        CreateMap<Building, BuildingDetailsResponse>();
        CreateMap<CreateBuildingRequest, Building>()
            .ForMember(e => e.Id, opt => opt.Ignore())
            ;
        
        CreateMap<UpdateBuildingRequest, Building>();

        // Images
        CreateMap<Image, ImageResponse>();
        CreateMap<Image, ImageResponseDetail>();
        
        // round sessions
        CreateMap<RoundSession, RoundSessionResponse>();
        CreateMap<RoundSnapshot, RoundSnapshotResponse>()
            .ForMember(e => e.RoundInfo, opt => opt.MapFrom(e => e.Data))
            ;

    }
}