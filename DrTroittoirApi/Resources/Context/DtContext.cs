using Microsoft.EntityFrameworkCore;
using Models;

namespace Resources.Context;

public class DtContext : DbContext
{
    public virtual DbSet<User> Users { get; set; }
    public virtual DbSet<Session> AuthSessions { get; set; }
    public virtual DbSet<Region> Regions { get; set; }
    public virtual DbSet<Round> Rounds { get; set; }
    public virtual DbSet<Building> Buildings { get; set; }
    public virtual DbSet<BuildingTask> BuildingTasks { get; set; }
    public virtual DbSet<BuildingInstruction> BuildingInstructions { get; set; }
    public virtual DbSet<BuildingTaskSession> BuildingTaskSessions { get; set; }
    public virtual DbSet<RoundSnapshot> RoundSnapshots { get; set; }
    public virtual DbSet<RoundSession> RoundSessions { get; set; }
    public virtual DbSet<Image> Images { get; set; }


    public DtContext(DbContextOptions<DtContext> options)
        : base(options)
    {
    }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        modelBuilder.Entity<User>(entity =>
        {
            entity
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            entity
                .HasMany<Session>(e => e.Sessions)
                .WithOne(e => e.User)
                .HasForeignKey(e => e.UserId);

            entity
                .HasMany<Region>(e => e.Regions)
                .WithMany(e => e.Users)
                ;
        });

        modelBuilder.Entity<Session>(entity =>
        {
            entity
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
        });

        modelBuilder.Entity<Round>(entity =>
        {
            entity
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            entity.HasOne<Region>(e => e.Region)
                .WithMany(r => r.Rounds)
                .HasForeignKey(r => r.RegionId);

            entity
                .HasMany<Building>(e => e.Buildings)
                .WithOne(e => e.Round)
                .HasForeignKey(e => e.RoundId);


            entity
                .HasMany<User>(e => e.AssignedUsers)
                .WithMany(e => e.Rounds);
        });

        modelBuilder.Entity<RoundSnapshot>(entity =>
        {
            entity
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            entity
                .HasOne<Round>(e => e.Round)
                .WithMany(e => e.Snapshots)
                .HasForeignKey(e => e.RoundId)
                .OnDelete(DeleteBehavior.Restrict);

            entity
                .HasOne<RoundSession>(e => e.RoundSession)
                .WithOne(e => e.RoundSnapshot)
                .HasForeignKey<RoundSnapshot>(e => e.RoundSessionId)
                .OnDelete(DeleteBehavior.Restrict);
        });

        modelBuilder.Entity<RoundSession>(entity =>
        {
            entity
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            entity
                .HasOne<Round>(e => e.Round)
                .WithMany(e => e.Sessions)
                .HasForeignKey(e => e.RoundId)
                .OnDelete(DeleteBehavior.Restrict);

            entity
                .HasOne<RoundSnapshot>(e => e.RoundSnapshot)
                .WithOne(e => e.RoundSession)
                .HasForeignKey<RoundSession>(e => e.RoundSnapshotId)
                .OnDelete(DeleteBehavior.Restrict);
        });

        modelBuilder.Entity<Image>(entity =>
        {
            entity
                .HasOne<User>(e => e.Uploader)
                .WithMany(e => e.UploadedImages)
                .HasForeignKey(e => e.UploaderId)
                .OnDelete(DeleteBehavior.Restrict);
        });

        modelBuilder.Entity<Building>(entity =>
        {
            entity
                .HasOne<Image>(e => e.Image)
                .WithMany()
                .HasForeignKey(e => e.ImageId)
                .OnDelete(DeleteBehavior.Restrict);
        });

        base.OnModelCreating(modelBuilder);
    }
}