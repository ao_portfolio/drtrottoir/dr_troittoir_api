﻿using Microsoft.Extensions.Configuration;
using Resources.Extensions;
using Azure.Data.Tables;

namespace Resources.Implementations;

public class AzureTableStorage : ITableResource
{
    private readonly IConfiguration _configuration;
    private TableServiceClient _client;
    
    public AzureTableStorage(IConfiguration configuration)
    {
        _configuration = configuration;
        
        var connectionString = _configuration.GetRequired("ConnectionStrings:StorageAccount");
        _client = new TableServiceClient(connectionString);
    }

    public async Task<T> GetByIdAsync<T>(string tableName, string partitionKey, string rowKey) where T : class, ITableEntity
    {
        var table = _client.GetTableClient(tableName);
        var entity = await table.GetEntityAsync<T>(partitionKey, rowKey);
        return entity.Value;
    }
    
    public async Task<T> InsertAsync<T>(string tableName, T entity) where T : class, ITableEntity
    {
        var table = _client.GetTableClient(tableName);
        await table.CreateIfNotExistsAsync();
        await table.AddEntityAsync(entity);
        return entity;
    }
    
    public async Task DeleteByIdAsync<T>(string tableName, string partitionKey, string rowKey) where T : class, ITableEntity
    {
        var table = _client.GetTableClient(tableName);
        await table.DeleteEntityAsync(partitionKey, rowKey);
    }
    
}