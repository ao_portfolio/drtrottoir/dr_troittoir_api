﻿using Azure.Core.Pipeline;
using Azure.Storage;
using Azure.Storage.Blobs;
using Azure.Storage.Blobs.Models;
using Azure.Storage.Sas;
using Microsoft.Extensions.Configuration;
using Models.Extensions;
using Resources.Extensions;

namespace Resources.Implementations;

public class AzureBlobStorage : IBlobStorage
{
    private readonly IConfiguration _configuration;
    private readonly BlobServiceClient _client;

    public AzureBlobStorage(IConfiguration configuration)
    {
        _configuration = configuration;
        var connectionString = _configuration.GetRequired("ConnectionStrings:StorageAccount");
        _client = new BlobServiceClient(connectionString);
    }

    public async Task<string> UploadAsync(string containerName, string blobName, Stream stream, string contentType = "application/octet-stream")
    {
        var container = _client.GetBlobContainerClient(containerName);
        await container.CreateIfNotExistsAsync();
        var blob = container.GetBlobClient(blobName);
        await blob.UploadAsync(stream, new BlobHttpHeaders {ContentType = contentType});
        return blob.Uri.ToString();
    }

    public async Task DeleteAsync(string containerName, string blobName)
    {
        var container = _client.GetBlobContainerClient(containerName);
        var blob = container.GetBlobClient(blobName);
        await blob.DeleteIfExistsAsync();
    }

    public async Task<Stream> DownloadAsync(string containerName, string blobName)
    {
        var container = _client.GetBlobContainerClient(containerName);
        var blob = container.GetBlobClient(blobName);
        var download = await blob.DownloadAsync();
        return download.Value.Content;
    }

    public async Task<bool> ExistsAsync(string containerName, string blobName)
    {
        var container = _client.GetBlobContainerClient(containerName);
        var blob = container.GetBlobClient(blobName);
        return await blob.ExistsAsync();
    }

    public async Task<string> GetUrlAsync(string containerName, string blobName)
    {
        var container = _client.GetBlobContainerClient(containerName);
        var blob = container.GetBlobClient(blobName);
        return blob.Uri.ToString();
    }

    public Task<Uri> GetAccessTokenForUri(Uri uri, string fileName, TimeSpan? expiry = null)
    {
        var client = GetBlobClient(uri);
        expiry ??= TimeSpan.FromMinutes(5);
        
        var sas = new BlobSasBuilder
        {
            ContentDisposition = fileName.IsNullOrEmpty() ? null : $"attachment; filename=\"{fileName}\"",
            StartsOn = DateTime.UtcNow - TimeSpan.FromMinutes(5),
            ExpiresOn = DateTime.UtcNow.Add(expiry.Value),
            BlobContainerName = client.BlobContainerName,
            BlobName = client.Name,
        };
        sas.SetPermissions(BlobSasPermissions.Read);
        
        var sasToken = sas.ToSasQueryParameters(new StorageSharedKeyCredential(
            GetKeyValueFromConnectionString("AccountName"), GetKeyValueFromConnectionString("AccountKey")));
        
        return Task.FromResult(new Uri($"{uri}?{sasToken}"));
    }
    
    private BlobClient GetBlobClient(Uri uri)
    {
        return new BlobClient(uri,
            new StorageSharedKeyCredential(GetKeyValueFromConnectionString("AccountName"),
                GetKeyValueFromConnectionString("AccountKey")), GetBlobClientOptions());
    }
    
    private static BlobClientOptions GetBlobClientOptions()
    {
        return new BlobClientOptions
        {
            Transport = new HttpClientTransport(new HttpClient {Timeout = TimeSpan.FromMinutes(2)}),
            Retry =
            {
                NetworkTimeout = TimeSpan.FromMinutes(2),
                MaxRetries = 30
            }
        };
    }

    private string GetKeyValueFromConnectionString(string key)
    {
        var connectionString = _configuration.GetRequired("ConnectionStrings:StorageAccount");
        var parts = connectionString.Split(";");
        var value = parts.First(part => part.StartsWith(key));
        return value.Substring(value.IndexOf('=') + 1);
    }
}