﻿using Azure.Data.Tables;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Resources.Extensions;
using StackExchange.Redis;

namespace Resources.Implementations;

public class RedisCacheTableStorage : ITableResource
{
    private readonly ConnectionMultiplexer _connectionMultiplexer;
    private readonly IDatabase _database;

    public RedisCacheTableStorage(IConfiguration configuration)
    {
        var connectionString = configuration.GetRequired("ConnectionStrings:Redis");
        _connectionMultiplexer = ConnectionMultiplexer.Connect(connectionString);
        _database = _connectionMultiplexer.GetDatabase();
    }

    public async Task<T> GetByIdAsync<T>(string tableName, string partitionKey, string rowKey)
        where T : class, ITableEntity
    {
        string key = $"{tableName}:{partitionKey}:{rowKey}";
        var value = await _database.StringGetAsync(key);

        if (value.IsNullOrEmpty)
            return null;
        
        return JsonConvert.DeserializeObject<T>(value);
    }

    public async Task<T> InsertAsync<T>(string tableName, T entity) where T : class, ITableEntity
    {
        var key = $"{tableName}:{entity.PartitionKey}:{entity.RowKey}";
        var value = JsonConvert.SerializeObject(entity);
        await _database.StringSetAsync(key, value);
        await _database.KeyExpireAsync(key, TimeSpan.FromHours(1));
        return entity;
    }

    public async Task DeleteByIdAsync<T>(string tableName, string partitionKey, string rowKey)
        where T : class, ITableEntity
    {
        var key = $"{tableName}:{partitionKey}:{rowKey}";
        await _database.KeyDeleteAsync(key);
    }
}