﻿using Google.Apis.Auth.OAuth2;
using Google.Cloud.Storage.V1;
using Microsoft.Extensions.Configuration;
using Models.Extensions;
using Resources.Extensions;

namespace Resources.Implementations;

public class GoogleCloudBucket : IBlobStorage
{
    private readonly StorageClient _storageClient;
    private readonly IConfiguration _configuration;
    private readonly GoogleCredential _googleCredential;
    
    public GoogleCloudBucket(IConfiguration configuration)
    {
        _configuration = configuration;
        _googleCredential = GoogleCredential.FromFile(_configuration.GetRequired("Google:StorageAuthFile"));
        _storageClient = StorageClient.Create(_googleCredential);
    }
    
    public async Task<string> UploadAsync(string containerName, string blobName, Stream stream, string contentType = "application/octet-stream")
    {
        var upload = await _storageClient.UploadObjectAsync("dt-dev-" + containerName, blobName, contentType, stream);
        return FilterUrl(upload.MediaLink);
    }

    public async Task DeleteAsync(string containerName, string blobName)
    {
        await _storageClient.DeleteObjectAsync(containerName, blobName);
    }

    public Task<Stream> DownloadAsync(string containerName, string blobName)
    {
        throw new NotImplementedException();
    }

    public async Task<bool> ExistsAsync(string containerName, string blobName)
    {
        var blob = await _storageClient.GetObjectAsync(containerName, blobName);
        return blob != null;
    }

    public async Task<string> GetUrlAsync(string containerName, string blobName)
    {
        var blob = await _storageClient.GetObjectAsync(containerName, blobName);
        return FilterUrl(blob.MediaLink);
    }

    public async Task<Uri> GetAccessTokenForUri(Uri uri, string fileName, TimeSpan? expiry = null)
    {
        TimeSpan expiresIn = expiry ?? TimeSpan.FromMinutes(5);
        
        var sac = _googleCredential.UnderlyingCredential as ServiceAccountCredential;
        var urlSigner = UrlSigner.FromCredential(sac);

        var signedUrl = await urlSigner.SignAsync(GetBucketName(uri), GetFileName(uri), expiresIn);
        
        return new Uri(signedUrl);
    }
    
    private string GetBucketName(Uri uri)
    {
        // https://storage.googleapis.com/download/storage/v1/b/dt-dev-images/o/ac9ef5f5-c849-45b1-bc51-75f7662d657a.png?generation=1683239282746247&alt=media
        var asString = uri.ToString();
        asString = asString.Replace("https://storage.googleapis.com/download/storage/v1/b/", "");
        var bucketName = asString.Split("/")[0];
        return bucketName;
    }
    
    private string GetFileName(Uri uri)
    {
        var asString = uri.ToString();
        asString = asString.Replace("https://storage.googleapis.com/download/storage/v1/b/", "");
        var fileName = asString.Split("/")[^1];
        return fileName;
    }

    private string FilterUrl(string url)
    {
        var fileName = url.Split("?")[0];
        return fileName;
    }
}