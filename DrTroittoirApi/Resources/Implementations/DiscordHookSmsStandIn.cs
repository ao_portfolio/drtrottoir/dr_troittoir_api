﻿using System.Net;
using System.Text;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Resources.Extensions;

namespace Resources.Implementations;

public class DiscordHookSmsStandIn : ISmsResource
{
    private readonly IConfiguration _configuration;
    
    public DiscordHookSmsStandIn(IConfiguration configuration)
    {
        _configuration = configuration;
    }

    public async Task<bool> SendSmsAsync(string number, string message)
    {
        // send to discord webhook
        var webhookUrl = _configuration.GetRequired("Discord:WebhookUrl");
        var client = new HttpClient();
        var content = new StringContent(
            JsonConvert.SerializeObject(new
            {
                content = $"SMS to {number}: {message}"
            }),
            Encoding.UTF8,
            "application/json"
        );
        var responseMessage = await client.PostAsync(webhookUrl, content);
        return responseMessage.StatusCode == HttpStatusCode.OK;
    }
}