﻿using Microsoft.Extensions.Configuration;

namespace Resources.Extensions;

public static class ConfigurationExtensions
{
    public static string GetRequired(this IConfiguration configuration, string key)
    {
        var setting = configuration[key];
        if (string.IsNullOrWhiteSpace(setting))
            throw new Exception($"missing config key: {key}");

        return setting;
    }
    
    public static T GetRequired<T>(this IConfiguration configuration, string key)
    {
        var setting = configuration[key];
        if (string.IsNullOrWhiteSpace(setting))
            throw new Exception($"missing config key: {key}");

        return (T)Convert.ChangeType(setting, typeof(T));
    }
}