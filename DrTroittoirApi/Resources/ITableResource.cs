﻿using Azure.Data.Tables;

namespace Resources;

public interface ITableResource
{
    Task<T> GetByIdAsync<T>(string tableName, string partitionKey, string rowKey) where T : class, ITableEntity;
    Task<T> InsertAsync<T>(string tableName, T entity) where T : class, ITableEntity;
    Task DeleteByIdAsync<T>(string tableName, string partitionKey, string rowKey) where T : class, ITableEntity;
}