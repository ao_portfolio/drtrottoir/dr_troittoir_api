﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Resources.Migrations
{
    /// <inheritdoc />
    public partial class AddScheduleToBuilding : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "ScheduleString",
                table: "Buildings",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "ScheduleString",
                table: "Buildings");
        }
    }
}
