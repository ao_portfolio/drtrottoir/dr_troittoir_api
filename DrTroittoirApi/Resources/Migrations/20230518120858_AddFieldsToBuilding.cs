﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Resources.Migrations
{
    /// <inheritdoc />
    public partial class AddFieldsToBuilding : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "City",
                table: "Buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "InstructionsString",
                table: "Buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Zip",
                table: "Buildings",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "City",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "InstructionsString",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "Zip",
                table: "Buildings");
        }
    }
}
