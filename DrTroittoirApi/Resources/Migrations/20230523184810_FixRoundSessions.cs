﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Resources.Migrations
{
    /// <inheritdoc />
    public partial class FixRoundSessions : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuildingTaskSessions_RoundSession_RoundSessionId",
                table: "BuildingTaskSessions");

            migrationBuilder.DropForeignKey(
                name: "FK_RoundSession_RoundSnapshots_RoundSnapshotId",
                table: "RoundSession");

            migrationBuilder.DropForeignKey(
                name: "FK_RoundSession_Rounds_RoundId",
                table: "RoundSession");

            migrationBuilder.DropForeignKey(
                name: "FK_RoundSession_Users_UserId",
                table: "RoundSession");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoundSession",
                table: "RoundSession");

            migrationBuilder.RenameTable(
                name: "RoundSession",
                newName: "RoundSessions");

            migrationBuilder.RenameIndex(
                name: "IX_RoundSession_UserId",
                table: "RoundSessions",
                newName: "IX_RoundSessions_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoundSession_RoundSnapshotId",
                table: "RoundSessions",
                newName: "IX_RoundSessions_RoundSnapshotId");

            migrationBuilder.RenameIndex(
                name: "IX_RoundSession_RoundId",
                table: "RoundSessions",
                newName: "IX_RoundSessions_RoundId");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "RoundSessions",
                type: "text",
                nullable: true,
                oldClrType: typeof(string),
                oldType: "text");

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoundSessions",
                table: "RoundSessions",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingTaskSessions_RoundSessions_RoundSessionId",
                table: "BuildingTaskSessions",
                column: "RoundSessionId",
                principalTable: "RoundSessions",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RoundSessions_RoundSnapshots_RoundSnapshotId",
                table: "RoundSessions",
                column: "RoundSnapshotId",
                principalTable: "RoundSnapshots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoundSessions_Rounds_RoundId",
                table: "RoundSessions",
                column: "RoundId",
                principalTable: "Rounds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoundSessions_Users_UserId",
                table: "RoundSessions",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_BuildingTaskSessions_RoundSessions_RoundSessionId",
                table: "BuildingTaskSessions");

            migrationBuilder.DropForeignKey(
                name: "FK_RoundSessions_RoundSnapshots_RoundSnapshotId",
                table: "RoundSessions");

            migrationBuilder.DropForeignKey(
                name: "FK_RoundSessions_Rounds_RoundId",
                table: "RoundSessions");

            migrationBuilder.DropForeignKey(
                name: "FK_RoundSessions_Users_UserId",
                table: "RoundSessions");

            migrationBuilder.DropPrimaryKey(
                name: "PK_RoundSessions",
                table: "RoundSessions");

            migrationBuilder.RenameTable(
                name: "RoundSessions",
                newName: "RoundSession");

            migrationBuilder.RenameIndex(
                name: "IX_RoundSessions_UserId",
                table: "RoundSession",
                newName: "IX_RoundSession_UserId");

            migrationBuilder.RenameIndex(
                name: "IX_RoundSessions_RoundSnapshotId",
                table: "RoundSession",
                newName: "IX_RoundSession_RoundSnapshotId");

            migrationBuilder.RenameIndex(
                name: "IX_RoundSessions_RoundId",
                table: "RoundSession",
                newName: "IX_RoundSession_RoundId");

            migrationBuilder.AlterColumn<string>(
                name: "UserId",
                table: "RoundSession",
                type: "text",
                nullable: false,
                defaultValue: "",
                oldClrType: typeof(string),
                oldType: "text",
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_RoundSession",
                table: "RoundSession",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_BuildingTaskSessions_RoundSession_RoundSessionId",
                table: "BuildingTaskSessions",
                column: "RoundSessionId",
                principalTable: "RoundSession",
                principalColumn: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_RoundSession_RoundSnapshots_RoundSnapshotId",
                table: "RoundSession",
                column: "RoundSnapshotId",
                principalTable: "RoundSnapshots",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoundSession_Rounds_RoundId",
                table: "RoundSession",
                column: "RoundId",
                principalTable: "Rounds",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_RoundSession_Users_UserId",
                table: "RoundSession",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
