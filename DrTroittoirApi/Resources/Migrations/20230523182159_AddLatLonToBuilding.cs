﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Resources.Migrations
{
    /// <inheritdoc />
    public partial class AddLatLonToBuilding : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<decimal>(
                name: "Lat",
                table: "Buildings",
                type: "numeric",
                nullable: true);

            migrationBuilder.AddColumn<decimal>(
                name: "Lon",
                table: "Buildings",
                type: "numeric",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Lat",
                table: "Buildings");

            migrationBuilder.DropColumn(
                name: "Lon",
                table: "Buildings");
        }
    }
}
