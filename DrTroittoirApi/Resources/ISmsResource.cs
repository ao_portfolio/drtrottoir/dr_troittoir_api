﻿namespace Resources;

public interface ISmsResource
{
    Task<bool> SendSmsAsync(string number, string message);
}