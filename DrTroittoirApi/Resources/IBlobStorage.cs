﻿namespace Resources;

public interface IBlobStorage
{
    Task<string> UploadAsync(string containerName, string blobName, Stream stream, string contentType = "application/octet-stream");
    Task DeleteAsync(string containerName, string blobName);
    Task<Stream> DownloadAsync(string containerName, string blobName);
    Task<bool> ExistsAsync(string containerName, string blobName);
    Task<string> GetUrlAsync(string containerName, string blobName);
    Task<Uri> GetAccessTokenForUri(Uri uri, string fileName, TimeSpan? expiry = null);
}