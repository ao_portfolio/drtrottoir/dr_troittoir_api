﻿using ApiTests.Mock;
using Logic;
using Logic.Implementations;
using Models;
using Resources.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ApiTests;

[Collection("Sequential")]
public class RegionTest : BaseTest
{
    private RegionLogic _regionLogic;
    private string _tempId;

    public RegionTest() : base()
    {
        _regionLogic = new RegionLogic(_context);
    }

    [Fact]
    public async Task GetRegionsAsync_Valid()
    {
        var result = await _regionLogic.GetRegionsAsync();
        Assert.True(result.Count > 0);
    }

    [Fact]
    public async Task GetRegionByIdAsync_Valid()
    {
        var result = await _regionLogic.GetRegionByIdAsync("region1");
        Assert.Equal("Gent", result.Name);
    }

    [Fact]
    public async Task UpdateRegionAsync_Valid()
    {
        var region = new Region
        {
            Id = "region2",
            Name = "test123_updated",
        };
        var result = await _regionLogic.UpdateRegionAsync(region);
        Assert.Equal("test123_updated", result.Name);
    }

    [Fact]
    public async Task DeleteRegionAsync_Valid()
    {
        await _regionLogic.DeleteRegionAsync("region3");
        var result = await _regionLogic.GetRegionByIdAsync("region3");
        Assert.Null(result?.Name);
    }
    
    // create region
    [Fact]
    public async Task CreateRegionAsync_Valid()
    {
        var region = new Region
        {
            Id = "region4",
            Name = "test123",
        };
        var result = await _regionLogic.CreateRegionAsync(region);
        Assert.Equal("test123", result.Name);
        _tempId = result.Id;
    }
    

    [Fact]
    public async Task GetRegionsAsyncByIds_Valid()
    {
        var result = await _regionLogic.GetRegionsAsync(new List<string> { "region1", "region2" });
        Assert.True(result.Count > 0);
    }
}
