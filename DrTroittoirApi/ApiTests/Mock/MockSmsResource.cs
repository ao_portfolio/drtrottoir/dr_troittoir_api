﻿using Resources;

namespace ApiTests.Mock;

public class MockSmsResource : ISmsResource
{
    private List<string> _sentMessages = new();
    public Task<bool> SendSmsAsync(string number, string message)
    {
        _sentMessages.Add(message);
        return Task.FromResult(true);
    }
    
    public List<string> GetSentMessages()
    {
        return _sentMessages;
    }
    
    public void ClearSentMessages()
    {
        _sentMessages.Clear();
    }
    
    public string GetLastSentMessage()
    {
        return _sentMessages.Last();
    }
}