﻿using Google.Api;
using Microsoft.EntityFrameworkCore;
using Models;
using Resources.Context;
using System.Linq;

namespace ApiTests.Mock;

public static class MockDbContext
{
    public static DtContext GetDbContext()
    {
        var options = new DbContextOptionsBuilder<DtContext>()
            .UseInMemoryDatabase(databaseName: "InMemoryDb")
            .Options;

        var context = new DtContext(options);
        ResetDbContext(context);

        return context;
    }

    public static void ResetDbContext(DtContext context)
    {
        var options = new DbContextOptionsBuilder<DtContext>()
            .UseInMemoryDatabase(databaseName: "InMemoryDb")
            .Options;

        context = new DtContext(options);
        context.Database.EnsureDeleted();
        context.Database.EnsureCreated();
        Seed(context);
    }
    
    private static void Seed(DtContext context)
    {
        context.Users.AddRange(GetFakeEmployeeList().Except(context.Users));
        context.Buildings.AddRange(GetFakeBuildingList().Except(context.Buildings));
        context.Rounds.AddRange(GetFakeRoundList().Except(context.Rounds));
        context.Regions.AddRange(GetFakeRegionList().Except(context.Regions));
        context.Users.AddRange(GetFakeUserList().Except(context.Users));
        try
        {
            context.SaveChanges();
        } catch(ArgumentException ex) { }
        
    }

    private static List<User> GetFakeEmployeeList()
    {
        return new List<User>()
        {
            new User
            {
                Id = Guid.NewGuid().ToString(),
                FirstName = "John",
                LastName = "Doe",
                PhoneNumber = "+1234567890",
                Email = "john@doe"
            },
            new User
            {
                Id = Guid.NewGuid().ToString(),
                FirstName = "Jane",
                LastName = "Doe",
                PhoneNumber = "+1234567891",
                Email = "jane@doe"
            },
        };
    }

    private static List<Building> GetFakeBuildingList()
    {
        return new List<Building>()
        {
            new Building
            {
                Id = "building1",
                Name = "building1",
                Address = "test",
                Description = "building description 1",
                Image = null,
                ImageId = "imageId1",
                Tasks = new List<Models.BuildingTask>(),
                Instructions = new List<BuildingInstruction>(),
                Round = null,
                RoundId = "round1",
                ScheduleString = "{ \"even\":{\"0\":[], \"1\":[{\"type\":\"PMD\"},{\"type\":\"restafval\"}],\"2\":[{\"type\":\"GFT\"}],\"3\":[{\"type\":\"papier\"}],\"4\":[]}, \"odd\":{\"0\":[],  \"1\":[{\"type\":\"glas\"}], \"2\":[], \"3\":[{\"type\":\"restafval\"},{\"type\":\"PMD\"}],\"4\":[] }}"
            },
            new Building
            {
                Id = "building2",
                Name = "building2",
                Address = "test",
                Description = "building description 2",
                Image = null,
                ImageId = "image2",
                Tasks = new List<Models.BuildingTask>(),
                Instructions = new List<BuildingInstruction>(),
                Round = null,
                RoundId = "round2"
            },
            new Building
            {
                Id = "building3",
                Name = "building3",
                Address = "test",
                Description = "building description 3",
                Image = null,
                ImageId = "image2",
                Tasks = new List<Models.BuildingTask>(),
                Instructions = new List<BuildingInstruction>(),
                Round = null,
                RoundId = "round2"
            },
        };
    }

    private static List<Round> GetFakeRoundList()
    {
        return new List<Round>()
        {
            new Round
            {
                Id = "round1",
                Name = "round1",
                RegionId = "region1"
            },
            new Round
            {
                Id = "round2",
                Name = "round2",
                RegionId = "region1"
            },
            new Round
            {
                Id = "round3",
                Name = "round3",
                RegionId = "region1"
            },
        };
    }

    private static List<Region> GetFakeRegionList()
    {
        return new List<Region>()
        {
            new Region
            {
                Id = "region1",
                Name = "Gent",
            },
            new Region
            {
                Id = "region2",
                Name = "Antwerpen",
            },
            new Region
            {
                Id = "region3",
                Name = "Fake",
            },
        };
    }

    private static List<User> GetFakeUserList()
    {
        return new List<User>()
        {
            new User
            {
                Id = "user1",
                FirstName = "test",
                LastName = "testing",
                PhoneNumber = "1111111111",
                Role = Role.Student,
            },
            new User
            {
                Id = "user2",
                FirstName = "test",
                LastName = "testing",
                PhoneNumber = "2222222222",
                Role = Role.Student,
            }
        };
    }
}