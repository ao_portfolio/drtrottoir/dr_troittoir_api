﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Primitives;

namespace ApiTests.Mock;

public class MockConfiguration : IConfiguration
{
    private readonly Dictionary<string, string> _values;

    public MockConfiguration(Dictionary<string, string> values)
    {
        _values = values;
    }

    public IConfigurationSection GetSection(string key)
    {
        throw new NotImplementedException();
    }

    public IEnumerable<IConfigurationSection> GetChildren()
    {
        throw new NotImplementedException();
    }

    public IChangeToken GetReloadToken()
    {
        throw new NotImplementedException();
    }

    public string this[string key]
    {
        get => _values[key];
        set => throw new NotImplementedException();
    }
}