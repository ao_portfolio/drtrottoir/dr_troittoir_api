﻿using Azure.Data.Tables;
using Resources;

namespace ApiTests.Mock;

public class MockTableResource : ITableResource
{
    private readonly Dictionary<string, Dictionary<string, ITableEntity>> _tables = new();
    public Task<T> GetByIdAsync<T>(string tableName, string partitionKey, string rowKey) where T : class, ITableEntity
    {
        if (!_tables.ContainsKey(tableName))
        {
            return Task.FromResult<T>(null);
        }

        var table = _tables[tableName];
        var key = $"{partitionKey}-{rowKey}";
        if (!table.ContainsKey(key))
        {
            return Task.FromResult<T>(null);
        }

        return Task.FromResult((T)table[key]);
    }

    public Task<T> InsertAsync<T>(string tableName, T entity) where T : class, ITableEntity
    {
        if (!_tables.ContainsKey(tableName))
        {
            _tables[tableName] = new Dictionary<string, ITableEntity>();
        }

        var table = _tables[tableName];
        var key = $"{entity.PartitionKey}-{entity.RowKey}";
        table[key] = entity;

        return Task.FromResult(entity);
    }

    public Task DeleteByIdAsync<T>(string tableName, string partitionKey, string rowKey) where T : class, ITableEntity
    {
        if (!_tables.ContainsKey(tableName))
        {
            return Task.CompletedTask;
        }

        var table = _tables[tableName];
        var key = $"{partitionKey}-{rowKey}";
        if (!table.ContainsKey(key))
        {
            return Task.CompletedTask;
        }

        table.Remove(key);
        return Task.CompletedTask;
    }
}