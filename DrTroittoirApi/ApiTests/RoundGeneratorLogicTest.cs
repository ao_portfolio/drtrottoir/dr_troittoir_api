﻿using ApiTests.Mock;
using Logic;
using Logic.Implementations;
using Models;
using Resources.Context;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Xunit;

namespace ApiTests;

[Collection("Sequential")]
public class RoundGeneratorLogicTest : BaseTest
{
    private RoundGeneratorLogic _logic;
    private RoundLogic _roundLogic;
    private string _tempId;

    public RoundGeneratorLogicTest() : base()
    {
        _roundLogic = new RoundLogic(_context);
        _logic = new RoundGeneratorLogic(_context, _roundLogic);
    }

    //StartRound

    [Fact]
    public async Task StartRound_Valid()
    {
        RoundSession result;
        try
        {
            await _roundLogic.AssignUserToRound("round1", "user1");
            result = await _logic.StartRound("user1", "round1", new DateTime(2023,5,24));
            Assert.Equal("user1", result.User.Id);
        }
        catch (Exception ex)
        {
            Assert.Fail(ex.Message);
        }
    }

    [Fact]
    public async Task StartRound_WrongDay()
    {
        RoundSession result;
        try
        {
            await _roundLogic.AssignUserToRound("round1", "user1");
            result = await _logic.StartRound("user1", "round1", new DateTime(2023, 5, 25));
            Assert.Fail("Didn't throw exception about no visits");
        }
        catch (Exception ex)
        {
            Assert.Equal("No buildings to visit today", ex.Message);
        }
    }

    [Fact]
    public async Task StartRound_NoVisits()
    {
        RoundSession result;
        try
        {
            await _roundLogic.AssignUserToRound("round3", "user1");
            result = await _logic.StartRound("user1", "round3", new DateTime(2023, 5, 25));
            Assert.Fail("Didn't throw exception about no visits");
        }
        catch (Exception ex)
        {
            Assert.Equal("No buildings to visit today", ex.Message);
        }
    }

    [Fact]
    public async Task StartRound_RoundNotFound()
    {
        RoundSession result;
        try
        {
            result = await _logic.StartRound("user1", null, new DateTime(2023, 5, 25));
            Assert.Fail("Didn't throw exception about round");
        }
        catch (Exception ex)
        {
            Assert.Equal("Round not found", ex.Message);
        }
    }

    [Fact]
    public async Task StartRound_UserNotAssignedToRound()
    {
        RoundSession result;
        try
        {
            result = await _logic.StartRound("user1", "round2", new DateTime(2023, 5, 25));
            Assert.Fail("Didn't throw exception about unassigned user");
        }
        catch (Exception ex)
        {
            Assert.Equal("User is not assigned to round", ex.Message);
        }
    }

    //MyRoundsForDay

    [Fact]
    public async Task MyRoundsForDay_Valid()
    {
        await _roundLogic.AssignUserToRound("round1", "user1");
        var result = await _logic.MyRoundsForDay("user1", new DateTime(2023, 5, 24));
        Assert.True(result.Count > 0);
        Assert.Equal("user1", result.Last().AssignedUsers.Last().Id);
    }
    
    [Fact]
    public async Task GarbageSchedule_Valid()
    {
        await _roundLogic.AssignUserToRound("round1", "user1");
        var result = await _logic.GetGarbageScheduleForDay("user1", "round1", new DateTime(2023, 5, 24));
        Assert.True(result.Count > 0);
    }
}
