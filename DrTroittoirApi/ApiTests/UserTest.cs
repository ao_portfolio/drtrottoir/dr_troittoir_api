﻿using ApiTests.Mock;
using Logic;
using Logic.Implementations;
using Models;
using Resources.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ApiTests;

[Collection("Sequential")]
public class UserTest : BaseTest
{
    private UserLogic _userLogic;
    private string _tempId;

    public UserTest() : base()
    {
        _userLogic = new UserLogic(_context);
    }

    [Fact]
    public async Task AddUser_Valid()
    {
        var user = new User
        {
            FirstName = "test",
            LastName = "testing",
            PhoneNumber = "1234567888",
            Role = Role.Student,
        };
        var result = await _userLogic.AddUser(user);
        _tempId = result.Id;
        Assert.NotNull(result.Id);
    }

    [Fact]
    public async Task GetAllUsers_Valid()
    {
        var result = await _userLogic.GetAllUsers();
        Assert.True(result.Count > 0);
    }

    [Fact]
    public async Task GetUserById_Valid()
    {
        var result = await _userLogic.GetUserById("user1");
        Assert.Equal("1111111111", result.PhoneNumber);
    }

    [Fact]
    public async Task GetUserByPhone_Valid()
    {
        var result = await _userLogic.GetUserByPhone("1111111111");
        Assert.Equal("user1", result.Id);
    }

    [Fact]
    public async Task UpdateUser_Valid()
    {
        var user = new User
        {
            FirstName = "test_updated",
            LastName = "testing",
            PhoneNumber = "1234567888",
            Role = Role.Student,
        };
        var result = await _userLogic.UpdateUser(user);
        Assert.Equal("test_updated", result.FirstName);
    }
}
