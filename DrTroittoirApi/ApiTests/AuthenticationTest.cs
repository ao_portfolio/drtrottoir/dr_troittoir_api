﻿using ApiTests.Mock;
using Logic.Implementations;
using Microsoft.Extensions.Configuration;
using Resources;
using Resources.Context;
using Xunit;
using Xunit.Abstractions;

namespace ApiTests;

[Collection("Sequential")]
public class AuthenticationTest : BaseTest
{
    [Fact]
    public async Task SmsAuth_CreateSession_Valid()
    {
        //MockDbContext.ResetDbContext(_context);
        var authLogic = new AuthLogic(_smsResource, _tableResource, _configuration, _context);
        var result = await authLogic.CreateAuthSessionAsync("+1234567890");
        Assert.NotNull(result);
    }
    
    [Fact]
    public async Task SmsAuth_CompleteSession_Valid()
    {
        //MockDbContext.ResetDbContext(_context);
        var authLogic = new AuthLogic(_smsResource, _tableResource, _configuration, _context);
        var session = await authLogic.CreateAuthSessionAsync("+1234567890");
        var result = await authLogic.CompleteAuthSessionAsync(session.PartitionKey, session.RowKey, session.Code);
        Assert.True(result);
    }
    
    [Fact]
    public async Task SmsAuth_CompleteSession_Invalid()
    {
        //MockDbContext.ResetDbContext(_context);
        var authLogic = new AuthLogic(_smsResource, _tableResource, _configuration, _context);
        var session = await authLogic.CreateAuthSessionAsync("+1234567890");
        var result = await authLogic.CompleteAuthSessionAsync(session.PartitionKey, session.RowKey, "1234");
        Assert.False(result);
    }
    
    [Fact]
    public async Task CreateAuthSessionAsyncForUser_GeneratesCode()
    {
        //MockDbContext.ResetDbContext(_context);
        var authLogic = new AuthLogic(_smsResource, _tableResource, _configuration, _context);
        var session = await authLogic.CreateTokenSessionForUser("+1234567890", "test");
        Assert.NotNull(session.token);
        Assert.NotNull(session.accesToken);
    }
    
    [Fact]
    public async Task ExtentAuthSessionAsyncForUser_GeneratesCode()
    {
        //MockDbContext.ResetDbContext(_context);
        var authLogic = new AuthLogic(_smsResource, _tableResource, _configuration, _context);
        var session = await authLogic.CreateTokenSessionForUser("+1234567890", "test");
        var result = await authLogic.ExtentTokenSessionForUser("+1234567890", "test", session.token);
        Assert.NotNull(result);
    }
    
    [Fact]
    public async Task SmsAuth_SendsSmsWithCode()
    {
        //MockDbContext.ResetDbContext(_context);
        var smsResource = new MockSmsResource();
        var authLogic = new AuthLogic(smsResource, _tableResource, _configuration, _context);
        var session = await authLogic.CreateAuthSessionAsync("+1234567890");
        Assert.Contains(session.Code, smsResource.GetLastSentMessage());
    }
    
    [Fact]
    public async Task SmsAuth_Logout()
    {
        var authLogic = new AuthLogic(_smsResource, _tableResource, _configuration, _context);
        var session = await authLogic.CreateTokenSessionForUser("1111111111", "test");
        await authLogic.Logout("user1", session.token);
        
        try
        {
            await authLogic.ExtentTokenSessionForUser("+1234567890", "test", session.token);
            Assert.Fail("Didn't throw exception about no visits");
        }
        catch (Exception ex)
        {
            Assert.Equal("Session not found", ex.Message);
        }
    }
    
    
}