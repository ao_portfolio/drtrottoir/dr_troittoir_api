﻿using ApiTests.Mock;
using Logic;
using Logic.Implementations;
using Models;
using Resources.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ApiTests;

[Collection("Sequential")]
public class RoundTest : BaseTest
{
    private RoundLogic _roundLogic;
    private string _tempId;

    public RoundTest() : base()
    {
        _roundLogic = new RoundLogic(_context);
    }

    [Fact]
    public async Task CreateRoundAsync_Valid()
    {
        var round = new Round
        {
            Name = "test",
            RegionId = "region1"
        };
        var result = await _roundLogic.CreateRoundAsync(round);
        _tempId = result.Id;
        Assert.NotNull(result.Id);
    }

    [Fact]
    public async Task GetRoundsAsync_Valid()
    {
        var result = await _roundLogic.GetRoundsAsync();
        Assert.True(result.Count > 0);
    }

    [Fact]
    public async Task GetDetailedRoundsAsync_Valid()
    {
        var result = await _roundLogic.GetRoundsAsync(false);
        Assert.True(result.Count > 0);
    }

    [Fact]
    public async Task GetRoundByIdAsync_Valid()
    {
        var result = await _roundLogic.GetRoundByIdAsync("round1");
        Assert.Equal("round1", result.Name);
    }

    [Fact]
    public async Task GetDetailedRoundByIdAsync_Valid()
    {
        var result = await _roundLogic.GetRoundByIdAsync("round1", false);
        Assert.Equal("round1", result.Name);
    }

    [Fact]
    public async Task UpdateRoundAsync_Valid()
    {
        var round = new Round
        {
            Id = "round1",
            Name = "test_updated",
            RegionId = "region1"
        };
        var result = await _roundLogic.UpdateRoundAsync(round);
        Assert.Equal("test_updated", result.Name);
    }

    [Fact]
    public async Task DeleteRoundAsync_Valid()
    {
        await _roundLogic.DeleteRoundAsync("round2");
        var result = await _roundLogic.GetRoundByIdAsync("round2");
        Assert.Null(result?.Name);
    }

    [Fact]
    public async Task AssignUserToRound_Valid()
    {
        try
        {
            await _roundLogic.AssignUserToRound("round1", "user1");
        }
        catch(Exception ex) {
            Assert.Fail(ex.Message);
        }
        var result = await _roundLogic.GetRoundByIdAsync("round1");
        Assert.True(result.AssignedUsers.Count > 0);
        Assert.Equal("user1", result.AssignedUsers.Last().Id);
    }

    [Fact]
    public async Task AssignUserToRound_RoundNotFound()
    {
        try
        {
            await _roundLogic.AssignUserToRound(null, "user1");
            Assert.Fail("Didn't throw exception about round");
        }
        catch (Exception ex)
        {
            Assert.Equal("Round not found",ex.Message);
        }
    }

    [Fact]
    public async Task AssignUserToRound_UserNotFound()
    {
        try
        {
            await _roundLogic.AssignUserToRound("round1", null);
            Assert.Fail("Didn't throw exception about user");
        }
        catch (Exception ex)
        {
            Assert.Equal("User not found", ex.Message);
        }
    }

    [Fact]
    public async Task UnAssignUserToRound_Valid()
    {
        await _roundLogic.AssignUserToRound("round1", "user1");
        try
        {
            await _roundLogic.UnAssignUserFromRound("round1", "user1");
        }
        catch (Exception ex)
        {
            Assert.Fail(ex.Message);
        }
        var result = await _roundLogic.GetRoundByIdAsync("round1");
        Assert.True(result.AssignedUsers.Count == 0);
    }

    [Fact]
    public async Task UnAssignUserFromRound_RoundNotFound()
    {
        try
        {
            await _roundLogic.UnAssignUserFromRound(null, "user1");
            Assert.Fail("Didn't throw exception about round");
        }
        catch (Exception ex)
        {
            Assert.Equal("Round not found", ex.Message);
        }
    }

    [Fact]
    public async Task UnAssignUserFromRound_UserNotFound()
    {
        try
        {
            await _roundLogic.UnAssignUserFromRound("round1", null);
            Assert.Fail("Didn't throw exception about user");
        }
        catch (Exception ex)
        {
            Assert.Equal("User not found", ex.Message);
        }
    }

    [Fact]
    public async Task UnAssignUserFromRound_UserNotAssignedToRound()
    {
        try
        {
            await _roundLogic.UnAssignUserFromRound("round2", "user1");
            Assert.Fail("Didn't throw exception about unassigned user");
        }
        catch (Exception ex)
        {
            Assert.Equal("User is not assigned to round", ex.Message);
        }
    }

    [Fact]
    public async Task MyRounds_Valid()
    {
        await _roundLogic.AssignUserToRound("round1", "user2");
        var result = await _roundLogic.MyRounds("user2");
        Assert.True(result.Count > 0);
        Assert.Equal("user2", result.Last().AssignedUsers.Last().Id);
    }
}
