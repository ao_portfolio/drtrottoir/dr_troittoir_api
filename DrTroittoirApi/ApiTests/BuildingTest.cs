﻿using ApiTests.Mock;
using Logic;
using Logic.Implementations;
using Models;
using Resources.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Xunit;

namespace ApiTests;

[Collection("Sequential")]
public class BuildingTest : BaseTest
{
    private BuildingLogic _buildingLogic;

    public BuildingTest() : base()
    {
        _buildingLogic = new BuildingLogic(_context);
    }

    [Fact]
    public async Task CreateBuildingAsync_Valid()
    {
        var building = new Building
        {
            Name = "test",
            Address = "test",
            Description = "test",
            Image = null,
            ImageId = "test",
            Tasks = new List<Models.BuildingTask>(),
            Instructions = new List<BuildingInstruction>(),
            Round = null,
            RoundId = "round1",
        };
        var result = await _buildingLogic.CreateBuildingAsync(building);
        Assert.NotNull(result.Id);
    }

    [Fact]
    public async Task GetBuildingsAsync_Valid()
    {
        var result = await _buildingLogic.GetBuildingsAsync();
        Assert.True(result.Count > 0);
    }

    [Fact]
    public async Task GetBuildingByIdAsync_Valid()
    {
        var result = await _buildingLogic.GetBuildingByIdAsync("building1");
        Assert.Equal("building description 1", result.Description);
    }

    [Fact]
    public async Task Geocode_Valid()
    {
        var result = await _buildingLogic.Geocode("gebroeders+de+smetstraat+1");
        Assert.Contains("3.7", result);
        Assert.Contains("51.0", result);
    }

    [Fact]
    public async Task UpdateBuildingAsync_Valid()
    {
        var building = new Building
        {
            Id = "building1",
            Name = "test123_updated",
            Address = "testt",
            Description = "testt",
            Image = null,
            ImageId = "testt",
            Tasks = new List<Models.BuildingTask>(),
            Instructions = new List<BuildingInstruction>(),
            Round = null,
            RoundId = "testt"
        };
        var result = await _buildingLogic.UpdateBuildingAsync(building);
        Assert.Equal("test123_updated", result.Name);
    }

    [Fact]
    public async Task DeleteBuildingAsync_Valid()
    {
        await _buildingLogic.DeleteBuildingAsync("building3");
        var result = await _buildingLogic.GetBuildingByIdAsync("building3");
        Assert.Null(result?.Name);
    }
}
