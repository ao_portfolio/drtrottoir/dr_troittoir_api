﻿using ApiTests.Mock;
using Resources.Context;

namespace ApiTests;

public abstract class BaseTest
{
    protected MockConfiguration _configuration;
    protected DtContext _context;
    protected MockSmsResource _smsResource;
    protected MockTableResource _tableResource;

    public BaseTest()
    {
        _context = MockDbContext.GetDbContext();
        _smsResource = new MockSmsResource();
        _tableResource = new MockTableResource();
        // Jwt:Issuer, Jwt:Audience, Jwt:Key
        var config = new Dictionary<string, string>()
        {
            {"Jwt:Issuer", "test"},
            {"Jwt:Audience", "test"},
            {"Jwt:Key", "testtesttesttesttesttesttest"},
            {"Jwt:LifeSpanMinutes", "5"},
            {"Jwt:AccessTokenLifeMinutes", "5"},
            
        };
        _configuration = new MockConfiguration(config);
    }
}