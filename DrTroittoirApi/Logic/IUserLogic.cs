﻿using Models;

namespace Logic;

public interface IUserLogic
{
    Task<User> GetUserByPhone(string phoneNumber);
    Task<User> AddUser(User user);
    Task<User> UpdateUser(User user);
    Task<List<User>> GetAllUsers();
    Task<User> GetUserById(string id);
}