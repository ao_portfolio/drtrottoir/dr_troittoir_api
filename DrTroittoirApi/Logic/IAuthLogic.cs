﻿using System.Security.Claims;
using Logic.Implementations;

namespace Logic;

public interface IAuthLogic
{
    Task<AuthSession> CreateAuthSessionAsync(string phoneNumber);
    Task<bool> CompleteAuthSessionAsync(string requestPhoneNumber, string clientSecret, string requestCode);
    Task<(string token, string accesToken)> CreateTokenSessionForUser(string phoneNumber, string deviceInfo);
    Task<(string token, string accesToken)> ExtentTokenSessionForUser(string phoneNumber, string deviceInfo,
        string refreshToken);
    ClaimsPrincipal GetPrincipalFromExpiredToken(string token);
    Task Logout(string userId, string token);
}