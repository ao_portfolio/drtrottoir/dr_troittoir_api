﻿using Logic.Models.ScheduleModels;
using Models;

namespace Logic;

public interface IRoundGeneratorLogic
{
    Task<List<Round>> MyRoundsForDay(string userId, DateTime day);
    Task<RoundSession> StartRound(string userId, string roundId, DateTime day);
    Task<Dictionary<string, Dictionary<string, List<Garbage>>>> GetGarbageScheduleForDay(string userId, string roundId, DateTime day);
}