﻿using Models;

namespace Logic;

public interface IRoundLogic
{
    Task<List<Round>> GetRoundsAsync(bool simple = true);
    Task<Round?> GetRoundByIdAsync(string id, bool simple = true);
    Task<Round> CreateRoundAsync(Round round);
    Task<Round> UpdateRoundAsync(Round round);
    Task DeleteRoundAsync(string id);
    Task AssignUserToRound(string roundId, string userId);
    Task UnAssignUserFromRound(string roundId, string userId);
    Task<List<Round>> MyRounds(string userId);
}