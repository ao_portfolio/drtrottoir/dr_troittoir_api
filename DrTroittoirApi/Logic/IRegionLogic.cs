﻿using Models;

namespace Logic;

public interface IRegionLogic
{
    Task DeleteRegionAsync(string id);
    Task<List<Region>> GetRegionsAsync();
    Task<Region> GetRegionByIdAsync(string id);
    Task<Region> CreateRegionAsync(Region region);
    Task<Region> UpdateRegionAsync(Region region);
    Task<List<Region>> GetRegionsAsync(List<string> ids);
}