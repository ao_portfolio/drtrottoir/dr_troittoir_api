﻿using Azure;
using Azure.Data.Tables;

namespace Logic.Implementations;

public class AuthSession : ITableEntity
{
    public string PartitionKey { get; set; }
    public string RowKey { get; set; }
    public DateTimeOffset? Timestamp { get; set; }
    public ETag ETag { get; set; }

    public string PhoneNumber => PartitionKey;
    public string ClientSecret => RowKey;
    public string Code { get; set; }

    public DateTime ExpiresAt { get; set; }
    public DateTime CreatedAt { get; set; }

    public bool IsExpired => DateTime.UtcNow > ExpiresAt;

    public bool IsValid(string code)
    {
        return Code == code && !IsExpired;
    }
}