﻿namespace Logic.Models.ScheduleModels;

public class GarbageSchedule
{
    public Dictionary<string, List<Garbage>> even { get; set; }
    public Dictionary<string, List<Garbage>> odd { get; set; }
}

public class Garbage
{
    public string Type { get; set; }
}