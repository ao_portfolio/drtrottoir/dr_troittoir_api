﻿using Models;
using Image = Models.Image;

namespace Logic;

public interface IImageLogic
{
    Task<Image> UploadImageAsync(Stream stream, string fileName, string uploaderId);
    Task<string> GetImageUrlWithToken(string url, string filename);
    Task<List<Image>> GetRecentImages();
}