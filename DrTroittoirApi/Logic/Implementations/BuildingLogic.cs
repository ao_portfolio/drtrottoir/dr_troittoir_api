﻿using Microsoft.EntityFrameworkCore;
using Models;
using Resources.Context;
using System.Net.Http;
using System.Net.Http.Json;

namespace Logic.Implementations;

public class BuildingLogic : IBuildingLogic
{
    private readonly DtContext _context;
    private readonly HttpClient _httpClient;

    public BuildingLogic(DtContext context)
    {
        _context = context;
        _httpClient = new HttpClient();
        _httpClient.DefaultRequestHeaders.Add("User-Agent", "Other");
    }

    public async Task<List<Building>> GetBuildingsAsync()
    {
        return await _context.Buildings
                .Include(e => e.Round)
                .ThenInclude(e => e.Region)
                .ToListAsync()
            ;
    }

    public async Task<Building?> GetBuildingByIdAsync(string id)
    {
        return await _context.Buildings
                .Include(e => e.Round)
                .ThenInclude(e => e.Region)
                .Include(e => e.Image)
                .FirstOrDefaultAsync(e => e.Id == id)
            ;
    }

    public async Task<string> Geocode(string query)
    {
        try
        {
            var url = "http://nominatim.openstreetmap.org/search?format=json&polygon_geojson=1&addressdetails=0&countrycodes=be&limit=1&q=";
            
            HttpResponseMessage response = await _httpClient.GetAsync(url+query);

            response.EnsureSuccessStatusCode();

            string responseContent = await response.Content.ReadAsStringAsync();
            string coordinatesJson = responseContent.Substring(responseContent.IndexOf("coordinates"));

            int startIndex = coordinatesJson.IndexOf('[') + 1;
            int length = coordinatesJson.IndexOf(']') - startIndex;
            string coordinatesString = coordinatesJson.Substring(startIndex,length);

            return coordinatesString;
        }
        catch (Exception ex)
        {
            Console.WriteLine($"Error occurred: {ex.Message}");
            return null;
        }
    }

    public async Task<Building> CreateBuildingAsync(Building building)
    {
        _context.Buildings.Add(building);
        await _context.SaveChangesAsync();
        return building;
    }

    public async Task<Building> UpdateBuildingAsync(Building building)
    {
        _context.Buildings.Update(building);
        await _context.SaveChangesAsync();
        return building;
    }

    public async Task DeleteBuildingAsync(string id)
    {
        var building = await _context.Buildings.FindAsync(id);
        if (building == null) return;
        _context.Buildings.Remove(building);
        await _context.SaveChangesAsync();
    }
}