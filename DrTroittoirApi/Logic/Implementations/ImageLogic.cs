﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Models;
using Models.Constants;
using Resources;
using Resources.Context;
using Resources.Extensions;
using SixLabors.ImageSharp.Metadata;
using Image = Models.Image;

namespace Logic.Implementations;

public class ImageLogic : IImageLogic
{
    private readonly DtContext _context;
    private readonly IBlobStorage _blobStorage;
    private readonly IConfiguration _configuration;

    public ImageLogic(DtContext context, IBlobStorage blobStorage, IConfiguration configuration)
    {
        _context = context;
        _blobStorage = blobStorage;
        _configuration = configuration;
    }

    public async Task<Image> UploadImageAsync(Stream stream, string fileName, string uploaderId)
    {
        var maxFileSize = 1024 * 1024 * _configuration.GetRequired<int>("Images:MaxFileSizeMb");
        
        if (stream.Length > maxFileSize)
            throw new Exception("File is too large");
        
        try
        {
            using var image = await SixLabors.ImageSharp.Image.LoadAsync(stream);
            var meta = StripMetadataAsync(image);
            var OrginalStream = new MemoryStream();
            await image.SaveAsPngAsync(OrginalStream);
            var thumbnail = await CreateThumbnailAsync(image);
            
            return await SaveImageAsync(OrginalStream, thumbnail, meta, fileName, uploaderId);
        }
        catch (Exception e)
        {
            Console.WriteLine(e);
            throw new Exception("File is not an image");
        }
    }

    public async Task<string> GetImageUrlWithToken(string url, string filename)
    {
        var uri = await _blobStorage.GetAccessTokenForUri(new Uri(url), filename);
        return uri.ToString();
    }
    
    public async Task<List<Image>> GetRecentImages()
    {
        return await _context.Images.OrderByDescending(x => x.CreatedAt).Take(20).ToListAsync();
    }

    private async Task<Image> SaveImageAsync(Stream originalStream, Stream thumbnailStream, ImageMetadata meta, string fileName, string uploaderId)
    {
        originalStream.Position = 0;
        thumbnailStream.Position = 0;
        string contentType = "image/png";
        
        var image = new Image
        {
            Name = Path.GetFileNameWithoutExtension(fileName),
            OriginalUrl = await _blobStorage.UploadAsync(BlobContainers.Images, Guid.NewGuid() + ".png", originalStream, contentType),
            ThumbnailUrl = await _blobStorage.UploadAsync(BlobContainers.Images, Guid.NewGuid() + ".png", thumbnailStream, contentType),
            UploaderId = uploaderId,
            CreatedAt = DateTime.UtcNow,
        };
        await _context.Images.AddAsync(image);
        await _context.SaveChangesAsync();
        return image;
    }

    private ImageMetadata StripMetadataAsync(SixLabors.ImageSharp.Image image)
    {
        ImageMetadata meta = image.Metadata;
        image.Metadata.ExifProfile = null;
        image.Metadata.XmpProfile = null;
        return meta;
    }


    private async Task<Stream> CreateThumbnailAsync(SixLabors.ImageSharp.Image image)
    {
        var maxHeight = _configuration.GetRequired<int>("Images:ThumbnailSize:Height");
        var maxWidth = _configuration.GetRequired<int>("Images:ThumbnailSize:Width");

        if (image.Width > maxWidth)
            return await ResizeOnWidthAsync(image, maxWidth);

        if (image.Height > maxHeight)
            return await ResizeOnHeightAsync(image, maxHeight);

        var ms = new MemoryStream();
        await image.SaveAsPngAsync(ms);
        return ms;
    }

    private async Task<Stream> ResizeOnHeightAsync(SixLabors.ImageSharp.Image image, int height)
    {
        var width = (int) Math.Round((double) image.Width / image.Height * height);
        image.Mutate(x => x.Resize(width, height));
        var ms = new MemoryStream();
        await image.SaveAsPngAsync(ms);

        return ms;
    }

    private async Task<Stream> ResizeOnWidthAsync(SixLabors.ImageSharp.Image image, int width)
    {
        var height = (int) Math.Round((double) image.Height / image.Width * width);
        image.Mutate(x => x.Resize(width, height));
        var ms = new MemoryStream();
        await image.SaveAsPngAsync(ms);

        return ms;
    }
}