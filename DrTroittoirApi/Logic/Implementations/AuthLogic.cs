﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Models;
using Resources;
using Resources.Context;
using Resources.Extensions;

namespace Logic.Implementations;

public class AuthLogic : IAuthLogic
{
    private readonly ISmsResource _smsResource;
    private readonly ITableResource _tableResource;
    private readonly IConfiguration _configuration;
    private readonly DtContext _dbContext;

    public AuthLogic(ISmsResource smsResource, ITableResource tableResource, IConfiguration configuration, DtContext dbContext)
    {
        _smsResource = smsResource;
        _tableResource = tableResource;
        _configuration = configuration;
        _dbContext = dbContext;
    }

    public async Task<AuthSession> CreateAuthSessionAsync(string phoneNumber)
    {
        var code = GenerateCode();
        var session = new AuthSession
        {
            PartitionKey = phoneNumber,
            RowKey = GenerateClientSecret(),
            Code = code,
            ExpiresAt = DateTime.UtcNow.AddMinutes(5),
            CreatedAt = DateTime.UtcNow
        };
        session = await _tableResource.InsertAsync("AuthSessions", session);
        await _smsResource.SendSmsAsync(phoneNumber, $"Your code is `{code}`");
        return session;
    }

    public async Task<bool> CompleteAuthSessionAsync(string requestPhoneNumber, string clientSecret, string requestCode)
    {
        var session = await _tableResource.GetByIdAsync<AuthSession>("AuthSessions", requestPhoneNumber, clientSecret);

        if (session == null)
            return false;

        var isValid = session.IsValid(requestCode);
        
        // TODO add max attempts
        
        if (isValid)
            await _tableResource.DeleteByIdAsync<AuthSession>("AuthSessions", requestPhoneNumber, clientSecret);

        return isValid;
    }

    public async Task<(string token, string accesToken)> CreateTokenSessionForUser(string phoneNumber, string deviceInfo)
    {
        var user = await _dbContext.Users
            .FirstOrDefaultAsync(x => x.PhoneNumber == phoneNumber);

        if(user == null)
            throw new Exception("User not found");

        var token = CreateJwtToken(user);
        var refreshToken = GenerateRefreshToken();

        var session = new Session()
        {
            Id = Guid.NewGuid().ToString(),
            UserId = user.Id,

            AccessToken = token,
            RefreshToken = refreshToken,
            DeviceFingerprint = deviceInfo,
            Expires = DateTime.UtcNow.AddMinutes(_configuration.GetRequired<int>("Jwt:LifeSpanMinutes")),
        };
        
        await _dbContext.AuthSessions.AddAsync(session);
        await _dbContext.SaveChangesAsync();
        
        return (token, refreshToken);
    }

    public async Task<(string token, string accesToken)> ExtentTokenSessionForUser(string phoneNumber,
        string deviceInfo, string refreshToken)
    {
        var user = await _dbContext.Users
            .Include(e => e.Sessions.Where(x => x.DeviceFingerprint == deviceInfo && refreshToken == x.RefreshToken))
            .FirstOrDefaultAsync(x => x.PhoneNumber == phoneNumber);
        
        if(user == null)
            throw new Exception("User not found");
        
        if(!user.Sessions.Any())
            throw new Exception("Session not found");
        
        var session = user.Sessions.First();
        
        if(session.Expires < DateTime.UtcNow)
            throw new Exception("Session expired");
        
        var token = CreateJwtToken(user);
        var newRefreshToken = GenerateRefreshToken();
        
        session.AccessToken = token;
        session.RefreshToken = newRefreshToken;
        session.Expires = DateTime.UtcNow.AddMinutes(_configuration.GetRequired<int>("Jwt:LifeSpanMinutes"));
        
        await _dbContext.SaveChangesAsync();
        return (token, newRefreshToken);
    }
    
    public ClaimsPrincipal GetPrincipalFromExpiredToken(string token)
    {
        var key = _configuration.GetRequired("Jwt:Key");
        var tokenValidationParameters = new TokenValidationParameters
        {
            ValidateAudience = false,
            ValidateIssuer = false,
            ValidateIssuerSigningKey = true,
            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key)),
            ValidateLifetime = false
        };
        var tokenHandler = new JwtSecurityTokenHandler();
        SecurityToken securityToken;
        var principal = tokenHandler.ValidateToken(token, tokenValidationParameters, out securityToken);
        var jwtSecurityToken = securityToken as JwtSecurityToken;
        if (jwtSecurityToken == null || !jwtSecurityToken.Header.Alg.Equals(SecurityAlgorithms.HmacSha256, StringComparison.InvariantCultureIgnoreCase))
            throw new SecurityTokenException("Invalid token");
        return principal;
    }

    public async Task Logout(string userId, string token)
    {
        // find session and delete it
        var session = await _dbContext.AuthSessions
            .FirstOrDefaultAsync(e => e.UserId == userId && e.AccessToken == token);
        
        // TODO add a revoked tokens list
        
        if(session == null)
            throw new Exception("Session not found");
        
        _dbContext.AuthSessions.Remove(session);
        await _dbContext.SaveChangesAsync();
    }

    private string CreateJwtToken(User user)
    {
        var issuer = _configuration.GetRequired("Jwt:Issuer");
        var audience = _configuration.GetRequired("Jwt:Audience");
        var key = _configuration.GetRequired("Jwt:Key");
        var lifeSpanMinutes = _configuration.GetRequired<int>("Jwt:AccessTokenLifeMinutes");
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(key));
        
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
        var claims = new[]
        {
            new Claim(ClaimTypes.Name, user.PhoneNumber),
            new Claim(ClaimTypes.NameIdentifier, user.Id),
            new Claim(ClaimTypes.Role, user.Role.ToString()),
        };
        
        var token = new JwtSecurityToken(
            issuer,
            audience,
            claims,
            expires: DateTime.UtcNow.AddMinutes(lifeSpanMinutes),
            signingCredentials: credentials
        );
        
        return new JwtSecurityTokenHandler().WriteToken(token);
    }
    
    private string GenerateRefreshToken()
    {
        var guid = Guid.NewGuid();
        return guid.ToString("N");
    }

    private string GenerateCode()
    {
        var random = Random.Shared;
        var code = new StringBuilder();
        for (var i = 0; i < 6; i++)
        {
            code.Append(random.Next(0, 9));
        }

        return code.ToString();
    }

    private string GenerateClientSecret()
    {
        var guid = Guid.NewGuid();
        return guid.ToString("N");
    }
}