﻿using Microsoft.EntityFrameworkCore;
using Models;
using Resources.Context;

namespace Logic.Implementations;

public class RegionLogic : IRegionLogic
{
    private readonly DtContext _context;
    
    public RegionLogic(DtContext context)
    {
        _context = context;
    }
    
    public async Task<List<Region>> GetRegionsAsync()
    {
        return await _context.Regions.ToListAsync();
    }
    
    public async Task<Region?> GetRegionByIdAsync(string id)
    {
        return await _context.Regions.FindAsync(id);
    }
    
    public async Task<List<Region>> GetRegionsAsync(List<string> ids)
    {
        return await _context.Regions.Where(x => ids.Contains(x.Id)).ToListAsync();
    }

    public async Task<Region> CreateRegionAsync(Region region)
    {
        _context.Regions.Add(region);
        await _context.SaveChangesAsync();
        return region;
    }
    
    public async Task<Region> UpdateRegionAsync(Region region)
    {
        _context.Regions.Update(region);
        await _context.SaveChangesAsync();
        return region;
    }
    
    public async Task DeleteRegionAsync(string id)
    {
        var region = await _context.Regions.FindAsync(id);
        if (region == null) return;
        _context.Regions.Remove(region);
        await _context.SaveChangesAsync();
    }

}