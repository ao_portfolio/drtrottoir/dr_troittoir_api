﻿using Microsoft.EntityFrameworkCore;
using Models;
using Resources.Context;

namespace Logic.Implementations;

public class UserLogic : IUserLogic
{
    private readonly DtContext _dbContext;
    
    public UserLogic(DtContext dbContext)
    {
        _dbContext = dbContext;
    }
    
    public async Task<User> GetUserByPhone(string phoneNumber)
    {
        return await _dbContext.Users
            .Include(e => e.Regions)
            .FirstOrDefaultAsync(x => x.PhoneNumber == phoneNumber);
    }
    
    public async Task<List<User>> GetAllUsers()
    {
        return await _dbContext.Users
            .Include(e => e.Regions)
            .ToListAsync();
    }
    
    public async Task<User> GetUserById(string id)
    {
        return await _dbContext.Users
            .Include(e => e.Regions)
            .FirstOrDefaultAsync(x => x.Id == id);
    }
    
    public async Task<User> AddUser(User user)
    {
        await _dbContext.Users.AddAsync(user);
        await _dbContext.SaveChangesAsync();
        return user;
    }
    
    public async Task<User> UpdateUser(User user)
    {
        _dbContext.Users.Update(user);
        await _dbContext.SaveChangesAsync();
        return user;
    }
}