﻿using Microsoft.EntityFrameworkCore;
using Models;
using Resources.Context;

namespace Logic.Implementations;

public class RoundLogic : IRoundLogic
{
    private readonly DtContext _dbContext;

    public RoundLogic(DtContext dbContext)
    {
        _dbContext = dbContext;
    }

    public async Task<List<Round>> GetRoundsAsync(bool simple = true)
    {
        if (simple)
            return await GetSimpleRoundsAsync();
        return await GetDetailedRoundsAsync();
    }

    private async Task<List<Round>> GetDetailedRoundsAsync()
    {
        return await _dbContext.Rounds
            .Include(r => r.Region)
            .Include(r => r.Buildings)
            .Include(r => r.AssignedUsers)
            .ToListAsync();
    }

    private async Task<List<Round>> GetSimpleRoundsAsync()
    {
        return await _dbContext.Rounds
            .Include(r => r.Region)
            .ToListAsync();
    }

    public async Task<Round?> GetRoundByIdAsync(string id, bool simple = true)
    {
        if (simple)
            return await GetSimpleRoundByIdAsync(id);
        return await GetDetailedRoundByIdAsync(id);
    }

    private async Task<Round?> GetDetailedRoundByIdAsync(string id)
    {
        return await _dbContext.Rounds
            .Include(r => r.Region)
            .Include(r => r.Buildings)
            .Include(r => r.AssignedUsers)
            .FirstOrDefaultAsync(r => r.Id == id);
    }

    private async Task<Round?> GetSimpleRoundByIdAsync(string id)
    {
        return await _dbContext.Rounds
            .Include(r => r.Region)
            .FirstOrDefaultAsync(r => r.Id == id);
    }

    public async Task<Round> CreateRoundAsync(Round round)
    {
        _dbContext.Rounds.Add(round);
        await _dbContext.SaveChangesAsync();
        return round;
    }

    public async Task<Round> UpdateRoundAsync(Round round)
    {
        _dbContext.Rounds.Update(round);
        await _dbContext.SaveChangesAsync();
        return round;
    }

    public async Task DeleteRoundAsync(string id)
    {
        var round = await _dbContext.Rounds.FindAsync(id);
        if (round == null) return;
        _dbContext.Rounds.Remove(round);
        await _dbContext.SaveChangesAsync();
    }

    public async Task AssignUserToRound(string roundId, string userId)
    {
        var round = await _dbContext.Rounds
            .Include(e => e.AssignedUsers)
            .FirstOrDefaultAsync(e => e.Id == roundId);
        var user = await _dbContext.Users.FirstOrDefaultAsync(e => e.Id == userId);

        if (round == null)
            throw new Exception("Round not found");

        if (user == null)
            throw new Exception("User not found");

        round.AssignedUsers.Add(user);
        await _dbContext.SaveChangesAsync();
    }

    public async Task UnAssignUserFromRound(string roundId, string userId)
    {
        var round = await _dbContext.Rounds
            .Include(e => e.AssignedUsers)
            .FirstOrDefaultAsync(e => e.Id == roundId);
        var user = await _dbContext.Users.FirstOrDefaultAsync(e => e.Id == userId);

        if (round == null)
            throw new Exception("Round not found");

        if (user == null)
            throw new Exception("User not found");

        if (!round.AssignedUsers.Contains(user))
            throw new Exception("User is not assigned to round");

        round.AssignedUsers.Remove(user);
        await _dbContext.SaveChangesAsync();
    }
    
    public async Task<List<Round>> MyRounds(string userId)
    {
        return await _dbContext.Rounds
            .Include(r => r.Region)
            .Include(r => r.Buildings)
            .Where(r => r.AssignedUsers.Any(u => u.Id == userId))
            .ToListAsync();
    }
}