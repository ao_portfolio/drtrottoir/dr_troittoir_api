﻿using System.Globalization;
using Logic.Models.ScheduleModels;
using Microsoft.EntityFrameworkCore;
using Models;
using Newtonsoft.Json;
using Resources.Context;

namespace Logic.Implementations;

public class RoundGeneratorLogic : IRoundGeneratorLogic
{
    private readonly DtContext _dbContext;
    private readonly IRoundLogic _roundLogic;

    public RoundGeneratorLogic(DtContext dbContext, IRoundLogic roundLogic)
    {
        _dbContext = dbContext;
        _roundLogic = roundLogic;
    }

    public async Task<List<Round>> MyRoundsForDay(string userId, DateTime day)
    {
        var userRounds = await _roundLogic.MyRounds(userId);

        List<Round> todaysRounds = new();
        foreach (var round in userRounds)
        {
            var todayRound = GetRoundForDay(round, day);
            todaysRounds.Add(todayRound);
        }

        return todaysRounds.Where(e => e.Buildings.Count > 0).ToList();
    }

    public async Task<RoundSession> StartRound(string userId, string roundId, DateTime day)
    {
        var round = await _dbContext.Rounds
            .Include(r => r.Buildings)
            .ThenInclude(e => e.Image)
            .Include(r => r.AssignedUsers)
            .FirstOrDefaultAsync(r => r.Id == roundId);

        if (round == null) throw new Exception("Round not found");
        
        // detach round from context
        _dbContext.Entry<Round>(round).State = EntityState.Detached;
        
        // check if user is assigned to round
        if (round.AssignedUsers.All(e => e.Id != userId))
            throw new Exception("User is not assigned to round");
        
        // check if round is not already started
        var roundSession = await _dbContext.RoundSessions
            .Where(e => e.RoundId == roundId && e.UserId == userId)
            .Include(e => e.RoundSnapshot)
            .FirstOrDefaultAsync(e => e.RoundId == roundId && e.CompletedAt == null && e.StartedAt.Value.Date == day.ToUniversalTime().Date);
        if (roundSession != null)
            return roundSession;
        
        var todayRound = GetRoundForDay(round, day);
        
        // check if there are buildings to visit
        if (todayRound.Buildings.Count == 0)
            throw new Exception("No buildings to visit today");
        
        var user = await _dbContext.Users.FirstOrDefaultAsync(e => e.Id == userId);

        return await CreateRoundSession(todayRound, day, user);
    }
    
    public async Task<Dictionary<string, Dictionary<string, List<Garbage>>>> GetGarbageScheduleForDay(string userId, string roundId, DateTime day)
    {
        var round = await _dbContext.Rounds
            .Include(r => r.Buildings)
            .ThenInclude(e => e.Image)
            .Include(r => r.AssignedUsers)
            .FirstOrDefaultAsync(r => r.Id == roundId);

        if (round == null) throw new Exception("Round not found");
        
        // detach round from context
        _dbContext.Entry<Round>(round).State = EntityState.Detached;
        
        // check if user is assigned to round
        if (round.AssignedUsers.All(e => e.Id != userId))
            throw new Exception("User is not assigned to round");
        
        var scheduleGarbage = new Dictionary<string, Dictionary<string, List<Garbage>>>();

        foreach (var building in round.Buildings)
        {
            var scheduleString = building.ScheduleString;
            if (scheduleString == null) continue;

            var buildingSchedule = JsonConvert.DeserializeObject<GarbageSchedule>(scheduleString);
            
            var takeGarbageOut = GetGarbage(buildingSchedule, day.AddDays(1));
            var takeGarbageInside = GetGarbage(buildingSchedule, day);
            
            var buildingGarbage = new Dictionary<string, List<Garbage>>
            {
                {"takeGarbageOut", takeGarbageOut},
                {"takeGarbageInside", takeGarbageInside}
            };

            if (takeGarbageOut.Count > 0 || takeGarbageInside.Count > 0)
                scheduleGarbage.Add(building.Id, buildingGarbage);

        }
        
        return scheduleGarbage;
    }

    private async Task<RoundSession> CreateRoundSession(Round dayRound, DateTime time, User user)
    {
        // serialize round
        var roundString = JsonConvert.SerializeObject(dayRound, Formatting.None, new JsonSerializerSettings
        {
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        });
        
        var hash = HashHelper.CreateMD5(roundString);
        
        var snapshot = new RoundSnapshot
        {
            RoundId = dayRound.Id,
            SnapShotAt = DateTime.UtcNow,
            Hash = hash,
            Data = roundString,
        };
        
        var session = new RoundSession
        {
            User = user,
            RoundId = dayRound.Id,
            StartedAt = time.ToUniversalTime(),
            RoundSnapshot = snapshot,
        };
        
        await _dbContext.RoundSessions.AddAsync(session);
        await _dbContext.SaveChangesAsync();
        return session;
    }


    private Round GetRoundForDay(Round round, DateTime date)
    {
        List<Building> buildingsToVisit = new();

        foreach (var building in round.Buildings)
        {
            var scheduleString = building.ScheduleString;
            if (scheduleString == null) continue;

            var schedule = JsonConvert.DeserializeObject<GarbageSchedule>(scheduleString);

            var garbageToday = GetGarbage(schedule, date);
            var garbageTomorrow = GetGarbage(schedule, date.AddDays(1));

            if (garbageToday.Count > 0 || garbageTomorrow.Count > 0)
                buildingsToVisit.Add(building);
        }
        
        round.Buildings = buildingsToVisit;
        return round;
    }

    private List<Garbage> GetGarbage(GarbageSchedule schedule, DateTime date)
    {
        // get week number
        var weekNumber = ISOWeek.GetWeekOfYear(date);
        int dayOfWeek = ((int) date.DayOfWeek == 0) ? 7 : (int) date.DayOfWeek;
        var thisWeek = weekNumber % 2 == 0 ? schedule.even : schedule.odd;

        if (!thisWeek.ContainsKey(dayOfWeek.ToString())) return new List<Garbage>();
        var garbage = thisWeek[dayOfWeek.ToString()];
        return garbage;
    }
    
}

internal static class HashHelper
{
    public static string CreateMD5(string input)
    {
        // Use input string to calculate MD5 hash
        using (System.Security.Cryptography.MD5 md5 = System.Security.Cryptography.MD5.Create())
        {
            byte[] inputBytes = System.Text.Encoding.ASCII.GetBytes(input);
            byte[] hashBytes = md5.ComputeHash(inputBytes);

            return Convert.ToHexString(hashBytes); // .NET 5 +

            // Convert the byte array to hexadecimal string prior to .NET 5
            // StringBuilder sb = new System.Text.StringBuilder();
            // for (int i = 0; i < hashBytes.Length; i++)
            // {
            //     sb.Append(hashBytes[i].ToString("X2"));
            // }
            // return sb.ToString();
        }
    }
}