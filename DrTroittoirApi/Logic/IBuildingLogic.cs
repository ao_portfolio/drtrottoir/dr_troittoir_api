﻿using Models;

namespace Logic;

public interface IBuildingLogic
{
    Task<List<Building>> GetBuildingsAsync();
    Task<Building?> GetBuildingByIdAsync(string id);
    Task<string> Geocode(string query);
    Task<Building> CreateBuildingAsync(Building building);
    Task<Building> UpdateBuildingAsync(Building building);
    Task DeleteBuildingAsync(string id);
}