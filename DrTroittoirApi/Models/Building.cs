﻿using System.Text.Json.Serialization;

namespace Models;

public class Building : Entity
{
    public string Name { get; set; }
    public string Address { get; set; }
    public string? Description { get; set; }
    public string? City { get; set; }
    public string? Zip { get; set; }
    public decimal? Lon { get; set; }
    public decimal? Lat { get; set; }
    public string? InstructionsString { get; set; }
    public string? ScheduleString { get; set; }
    public string? ImageId { get; set; }
    public Image? Image { get; set; }
    public Round Round { get; set; }
    public string RoundId { get; set; }
    public List<BuildingTask> Tasks { get; set; }
    public List<BuildingInstruction> Instructions { get; set; }
}