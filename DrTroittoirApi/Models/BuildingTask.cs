﻿namespace Models;

public class BuildingTask : Entity
{
    public string Name { get; set; }
    public Building Building { get; set; }
    public List<BuildingTaskSession> TaskSessions { get; set; }
}