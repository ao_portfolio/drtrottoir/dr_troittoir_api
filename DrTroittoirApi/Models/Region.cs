﻿namespace Models;

public class Region : Entity
{
    public string Name { get; set; }
    public List<User> Users { get; set; }
    public List<Round> Rounds { get; set; }
}