﻿namespace Models;

public class BuildingTaskSession : Entity
{
    public BuildingTask Task { get; set; }
    public DateTime? StartedAt { get; set; }
    public DateTime? CompletedAt { get; set; }
    public bool IsCompleted => CompletedAt.HasValue;
}