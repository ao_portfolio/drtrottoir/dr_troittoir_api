﻿namespace Models;

public class BuildingInstruction : Entity
{
    public string Name { get; set; }
    public Building Building { get; set; }
}