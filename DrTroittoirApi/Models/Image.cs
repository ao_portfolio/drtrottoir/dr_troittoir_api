﻿namespace Models;

public class Image : Entity
{
    public string Name { get; set; }
    public string OriginalUrl { get; set; }
    public string ThumbnailUrl { get; set; }
    public User Uploader { get; set; }
    public string UploaderId { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime? ExpiresAt { get; set; }
}