﻿namespace Models;

public class Round : Entity
{
    public string Name { get; set; }
    public string RegionId { get; set; }
    public Region Region { get; set; }
    public List<Building> Buildings { get; set; }
    public List<RoundSnapshot> Snapshots { get; set; }
    public List<RoundSession> Sessions { get; set; }
    public List<User> AssignedUsers { get; set; }
}