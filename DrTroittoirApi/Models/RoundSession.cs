﻿namespace Models;

public class RoundSession : Entity
{
    public User User { get; set; }
    public string? UserId { get; set; }
    public Round? Round { get; set; }
    public string? RoundId { get; set; }
    public RoundSnapshot? RoundSnapshot { get; set; }
    public string? RoundSnapshotId { get; set; }
    public DateTime? StartedAt { get; set; }
    public DateTime? CompletedAt { get; set; }
    public bool IsCompleted => CompletedAt.HasValue;
    public List<BuildingTaskSession> Tasks { get; set; }
}