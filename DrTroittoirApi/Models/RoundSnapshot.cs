﻿namespace Models;

public class RoundSnapshot : Entity
{
    public Round? Round { get; set; }
    public string? RoundId { get; set; }
    public DateTime SnapShotAt { get; set; }
    public RoundSession? RoundSession { get; set; }
    public string? RoundSessionId { get; set; }
    public string Data { get; set; }
    public string Hash { get; set; }
}