﻿namespace Models;

public class User : Entity
{
    public string FirstName { get; set; }
    public string LastName { get; set; }
    public string Name => $"{FirstName} {LastName}";
    
    public string? Email { get; set; }
    public string PhoneNumber { get; set; }
    public Role Role { get; set; }
    public List<Session> Sessions { get; set; }
    public List<Region> Regions { get; set; }
    public List<Round> Rounds { get; set; }
    public List<RoundSession> RoundSessions { get; set; }
    public List<Image> UploadedImages { get; set; }
}