namespace Models;

public enum Role
{
    Admin,
    SuperStudent,
    Student,
}