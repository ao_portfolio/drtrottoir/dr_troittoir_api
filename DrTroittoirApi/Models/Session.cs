﻿namespace Models;

public class Session : Entity
{
    public string AccessToken { get; set; }
    public string RefreshToken { get; set; }
    public string DeviceFingerprint { get; set; }
    public DateTime Expires { get; set; }
    public User User { get; set; }
    public string UserId { get; set; }
    
    public bool IsValid()
    {
        return Expires > DateTime.UtcNow;
    }
}